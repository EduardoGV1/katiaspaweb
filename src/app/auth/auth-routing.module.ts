import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutLoginComponent } from './login/login.component';

const routes: Routes = [
    { path: "Login", component: LayoutLoginComponent },
    { path: "**", redirectTo: "Login"}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
