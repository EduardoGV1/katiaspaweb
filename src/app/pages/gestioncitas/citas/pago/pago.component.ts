import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NzDrawerRef } from "ng-zorro-antd";
import { IApiResponse } from "src/app/shared/models/response";
import { CitaService } from "src/app/shared/services/cita.service";
import { ObvsService } from "src/app/shared/services/obvs.service";
import { __messageSnackBar, __messageSnackBarLeft } from "src/app/utils/helpers";

@Component({
    selector:'app-pago',
    templateUrl:'./pago.component.html'
})

export class PagoComponent implements OnInit{
    formularioGroup: FormGroup;
    isPagado:boolean=false;
    bottonDisabled:boolean=true;
    labelPago="REGISTRAR PAGO"

    size='large'

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar :    MatSnackBar,
        private _citaService:CitaService,
        private dialogRef: MatDialogRef<PagoComponent>,
        public _obvsService: ObvsService,
    ){
        console.log("data",data)
        this.formularioGroup = new FormGroup({
            nombreCliente: new FormControl({ value: data.ClienteNombre, disabled: true }),
            nombreServicio: new FormControl({ value: data.ServicioNombre, disabled: true }),
            tipoPago:new FormControl(""),
            monto: new FormControl({ value: data.MontoServicio, disabled: true }),
            // correo:new FormControl({ value: "", disabled: false }),
            // MontoServicio
            
        })
    }

    ngOnInit(): void {
        debugger
        if (this.data.EstadoPago==1) {
            this.isPagado=true;
            this.formularioGroup.get('tipoPago').setValue(this.data.TipoPago.toString());
            this.formularioGroup.disable();
            this.labelPago="DETALLE DE PAGO"
        } else{
            this.bottonDisabled=false
        }
    }
    close(){
        this.dialogRef.close({data:false});
    }

    async registrarPago(){
        console.log("form validado")
        try {
            this.bottonDisabled=true
            this._obvsService.toogleSpinner()
            if(this.formularioGroup.get('tipoPago').value=="" || this.formularioGroup.get('tipoPago').value==null || this.formularioGroup.get('tipoPago').value==undefined){
                return __messageSnackBarLeft(this._matSnackBar, "Seleccione un Metodo de Pago")
            }
            const { Descripcion, Identificador, Resultado }: IApiResponse = await this._citaService.registrarPago(
                {
                    TipoPago:this.formularioGroup.get('tipoPago').value,
                    PagoServicioID:this.data.PagoServicioID,
                    fechaPago:new Date()
                }
            )
            console.log("Descripcion",Descripcion)
            if (Identificador=="success") {
                __messageSnackBar(this._matSnackBar, Descripcion)
                this.dialogRef.close(true);
            }
            if (Identificador=="error") {
                return __messageSnackBarLeft(this._matSnackBar, Descripcion)
            }
        } catch (error) {
            return __messageSnackBarLeft(this._matSnackBar, error)
        } finally {
            this._obvsService.toogleSpinner()
            this.bottonDisabled=false;
        }
    }
    

}