import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { IApiResponse } from "src/app/shared/models/response";
import { ObvsService } from "src/app/shared/services/obvs.service";
import { ServicioService } from "src/app/shared/services/servicio.service";

@Component({
    selector: 'app-detalle-clientes',
    templateUrl: './detalle-clientes.component.html',
    styleUrls: ["./detalle-clientes.component.scss"],
})

export class DetalleClientesComponent implements OnInit {

    clienteArreglo = []
    citasArreglo = []
    citasInfo = []

    formCliente: FormGroup
    ClienteID: number

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar: MatSnackBar,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        public dialog: MatDialog,
        public _obvsService: ObvsService
    ) {
        // this.crearClienteForm()

        this.route.params.subscribe(params => {
            this.ClienteID = +params.id
        })
    }


    async ngOnInit() {
        try {
            this._obvsService.toogleSpinner()
            await this.listarClientexID(this.ClienteID)
            await this.listarCitasxClienteID(this.ClienteID)
            await this.listarCitasInfo(this.ClienteID)
        }
        catch {

        }
        finally {
            this._obvsService.toogleSpinner()
        }
    }

    async listarClientexID(ClienteID: number) {

        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Resultado
            }: IApiResponse = await this._servicioService.obtenerClientexID({
                clienteID: ClienteID
            })
            if (Identificador == "success") {

                this.clienteArreglo = Resultado
                // this.formCliente.patchValue(this.clienteArreglo)
                console.log(Resultado)
            }
        }
        catch (error) {
            console.log(error)
        }
    }

    async listarCitasInfo(ClienteID: number) {

        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Resultado,
                CitasInfo
            }: IApiResponse = await this._servicioService.obtenerClientexID({
                clienteID: ClienteID
            })
            if (Identificador == "success") {

                this.citasInfo = CitasInfo
                console.log(CitasInfo)
            }
        }
        catch (error) {
            console.log(error)
        }
    }

    async listarCitasxClienteID(ClienteID: number) {
        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Citas }: IApiResponse = await this._servicioService.obtenerCitasxClienteID({
                    clienteID: ClienteID
                })
            if (Identificador == "success") {
                this.citasArreglo = Citas
                console.log(this.citasArreglo)
            }
        }

        catch (error) {
            console.log(error)
        }

    }
}