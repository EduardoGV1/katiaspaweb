import { Component, OnInit } from '@angular/core';
import { IApiResponse, IRoles } from '../../../shared/models/response';
import { __messageSnackBar } from '../../../utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { PersonsService } from 'src/app/shared/services/persons.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AgregarPersonalComponent } from './agregar/agregar-personal.component';
import { getRol } from '../../../utils/storage';
import { EditarPasswordComponent } from './editar-password/editar-password-personal.component';
import { FormControl, FormGroup } from '@angular/forms';
import { RolesService } from 'src/app/shared/services/roles.Service';
import { DatePipe } from "@angular/common";
import { ObvsService } from 'src/app/shared/services/obvs.service';

export interface DialogData {
  usuarioID: number,
  nombre: string,
  apellido: string
}

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html'
})
export class PersonalComponent implements OnInit {
  trabajadoresArr = []
  idUsuario: Number
  //nombre:string
  //apellido:string
  filtroForm: FormGroup;
  dateRange:any;
  rolesArr=[]
  displayedColumns: string[] = ['Nombre','Apellido','Genero','Nombre Rol','Fecha Registro','Activo','Acción'];
  paginatorPageSize = 10;
  paginatorTotalRecords=0;
  fehcasSelected:any[]=[]
  date:any[]=[];

  constructor(
    private _personsService: PersonsService,
    private _matSnackBar: MatSnackBar,
    public _route: Router,
    private _rolesService:RolesService,
    public _activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    public datePipe: DatePipe,
    public _obvsService: ObvsService,
  ) {
    this.filtroForm = new FormGroup({
      rol: new FormControl(""),
      rolID: new FormControl(null)
    })
  }

  async ngOnInit() {
    this.getRoles();
    this.listarTrabajadores(1,this.paginatorPageSize);
    
  }
  async listarTrabajadores(start:number,length:number){
    try {
      this._obvsService.toogleSpinner()
      const {
        Descripcion,
        Identificador,
        Resultado,
        Total
      }: IApiResponse = await this._personsService.listarTrabajadorPaginado({
        fechaFin: (this.date.length==2)?this.date[1]:null,
        fechaInicio: (this.date.length==2)?this.date[0]:null,
        rolID: (this.filtroForm.value.rolID!=null)?this.filtroForm.value.rolID:null,
        genero: 2,
        length: length,
        start: start,
        isAdmin: false,
      })
      if (Identificador == "error") {
        return __messageSnackBar(this._matSnackBar, Descripcion)
      }
      this.trabajadoresArr = Resultado
      this.paginatorTotalRecords=Total
      console.log(Resultado);
    } catch (error) {
      console.log(error)
    } finally{
      this._obvsService.toogleSpinner()
    }
  }
  limpiarFiltros(){
    this.filtroForm = new FormGroup({
      rol: new FormControl(""),
      rolID: new FormControl(null)
    })
    this.date=[];
  }
  onChangeRangeDate(event:any){
    console.log("event",event)
    this.date=event;
    console.log("this.date1",this.date)
  }
  onOk(event:Date[]){
    console.log(event)
}
async dev(event:any){
  console.log("event",event)
  await this.listarTrabajadores(event.pageIndex*this.paginatorPageSize,this.paginatorPageSize)

}

  async getRoles(){
    try {
      const{
          Mensaje,
          Identificador,
          Descripcion,
          Roles
      }:IRoles = await this._rolesService.listarRoles({
          Start:1,
          Length:20
      })
      this.rolesArr=Roles
      console.log(this.rolesArr)
  } catch (error) {
      console.log(error)
  }
  }

  editarTrabajador(usuarioID: number) {
    this._route.navigate(['editar', usuarioID], { relativeTo: this._activatedRoute })
  }
  agregarTrabajador() {
    const dialogRef = this.dialog.open(AgregarPersonalComponent, { disableClose: true })
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.changeData) this.ngOnInit()
    })
  }
  editarPassword(usuarioID: number, nombre: string, apellido: string) {
    console.log(usuarioID, nombre, apellido)
    const dialogRef = this.dialog.open(EditarPasswordComponent, {
      disableClose: true,
      data: { usuarioID: usuarioID, nombre: nombre, apellido: apellido }
    }
    )
    console.log(dialogRef)
    dialogRef.afterClosed().subscribe(() => {
      if (true) this.ngOnInit()
    })
  }
  async buscarPersonal() {
    this.filtroForm.value.apellido
    this.filtroForm.value.nombre,
    this.listarTrabajadores(1,this.paginatorPageSize);
    
  }

  //Definir horarios disponibles
  definirHorarios(usuarioID?: number){
    console.log("Horarios definidos")
    this._route.navigate(['horarios', usuarioID], { relativeTo: this._activatedRoute })
  }

  definirHorariosAll(){
    console.log("Horarios definidos All")
    this._route.navigate(['horarios'], { relativeTo: this._activatedRoute })
  }
  ngModelChange(event:any){
    console.log("event 2:::",event)
    console.log("this.date2",this.date)
  }
}
