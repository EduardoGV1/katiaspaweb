import { DatePipe } from "@angular/common";
import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { IPersonal } from "src/app/pages/citas/interface/interface";
import { DialogData } from "src/app/pages/persons/personal/personal.component";
import { Hora, Horario } from "src/app/shared/classes/horario";
import { IApiResponse } from "src/app/shared/models/response";
import { CitaService } from "src/app/shared/services/cita.service";
import { ObvsService } from "src/app/shared/services/obvs.service";
import { __messageSnackBar } from "src/app/utils/helpers";
import Swal from "sweetalert2";

@Component({
    selector: 'app-detalle-cita',
    templateUrl: './detalle-cita.component.html'
})

export class DetalleCitaComponent implements OnInit {
    formularioGroup: FormGroup
    agregarPenalidad: boolean = false;
    reprogramarSesion: boolean = false;
    sesionNroSelected: string = "Sesion Nro X";
    sesionSelected: any;
    today = new Date();
    horario = new Horario();
    listaHorarioOcupado: IPersonal[] = [];
    listaHorarioInicio: Hora[];
    duracionServicio: number = 0//obtener API
    fechaSeleccionadaString: string;
    horaSelected: any;
    selectedHoraFin: any;
    consultorioCarga: any[] = []
    consultorioIDCMin = 0;
    consultorioCargaMin = 0;
    positionLista = 0;
    reprogramado: boolean = false;
    programado: boolean = false
    sigSesion: boolean = false;
    isProgramarSesion: boolean = false;
    habilitadoButton: boolean = false;
    fechaSelectedHabilitacion: any;
    disabledDate = (current: Date): boolean => {
        return differenceInCalendarDays(this.today, current) + 1 > 0;
    };

    listaTrabajadores: any[] = [];
    listaSesiones: any[] = [];
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _citaService: CitaService,
        private _matSnackBar: MatSnackBar,
        public datePipe: DatePipe,
        private spinner: NgxSpinnerService,
        public _obvsService: ObvsService,
    ) {
        console.log("data", data)
        this.duracionServicio = data.duracion;
        this.formularioGroup = new FormGroup({
            monto: new FormControl(0.0, [Validators.required]),
            nombreCliente: new FormControl({ value: 'Eduardo Gonzales', disabled: true }),
            nombreServicio: new FormControl({ value: 'Masajes Reductores', disabled: true }),
            fechaSelected: new FormControl(null, [Validators.required]),
            trabajador: new FormControl('Juana Reyes', [Validators.required]),
            horaInicio: new FormControl(""),
            horaFin: new FormControl({ value: "", disabled: true }),
            fechaSelectedHabilitacion: new FormControl(null),

        })
    }

    ngOnInit(): void {
        //Listar sesiones
        this.obtenerSesionesAll();
        this.obtenerersonalxServicioID();



    }

    async obtenerSesionesAll() {
        try {
            this._obvsService.toogleSpinner()
            const { Descripcion, Identificador, Resultado, Sesiones }: IApiResponse = await this._citaService.obtenerSesionesxCitaWeb({ citaID: this.data.citaID });
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            debugger
            this.listaSesiones = Sesiones;
            const size = this.listaSesiones.length;
            // if (this.listaSesiones[size - 1].EstadoString == "En Espera" 
            // || this.listaSesiones[size - 1].EstadoString == "Cliente no asistido"
            // || this.listaSesiones[size - 1].EstadoString == "En espera de asignación"
            // || this.listaSesiones[size - 1].EstadoString == "Realizado"
            // || this.listaSesiones[size - 1].EstadoString == "Reprogramado por Negocio"
            // || this.listaSesiones[size - 1].EstadoString == "Reprogramado"
            // || this.listaSesiones[size - 1].EstadoString == "Cancelado") {
            this.listaSesiones[size - 1].IsReprogramable = true;
            if (this.listaSesiones[size - 1].Estado == '3' && this.data.sesionTotal == this.listaSesiones[size - 1].NSesion) {
                this.listaSesiones[size - 1].IsReprogramable = false;
            }
        } catch (error) {
            return __messageSnackBar(this._matSnackBar, error)
        } finally {
            this._obvsService.toogleSpinner()
        }

        // }

    }

    async cambiarEstadoCita(item: any, estadoTo: string) {
        if (!this.data.estadoPago) {
            return __messageSnackBar(this._matSnackBar, "Debe realizar el pago antes de iniciar la sesion");
        }
        try {
            this._obvsService.toogleSpinner()
            let data = {
                estado: estadoTo,
                citaSesionID: item.CitaSesionID
            }
            const { Identificador, Mensaje, Descripcion }: IApiResponse = await this._citaService.cambiarEstadoCitaSesion(data);
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion);
            }
            if (Identificador == "success") {
                this.obtenerSesionesAll();
                return __messageSnackBar(this._matSnackBar, Descripcion);
    
            }
        } catch (error) {

        } finally {
            this._obvsService.toogleSpinner()
        }
        
    }

    confirmarSigSesion(sesion: any) {
        debugger
        console.log("sig sesion success!!")
        this.sesionNroSelected = "Sesion Nro." + (Number(sesion.NSesion) + 1);
        this.sesionSelected = { citaSesionID: sesion.CitaSesionID, sesionNro: sesion.NSesion, usuarioOldID: sesion.UsuarioID, citaID: this.data.citaID, fechaSesionAnterior: sesion.FechaHora }
        this.sigSesion = true
        this.habilitadoButton = false
        this.disabledDate = (current: Date): boolean => {
            return differenceInCalendarDays(new Date(this.sesionSelected.fechaSesionAnterior), current) + 1 > 0;
        };
    }

    cancelSigSesion() {
        this.sigSesion = false
    }

    confirmarProgramacionSesion(sesion: any) {
        //
        this.sesionNroSelected = "Sesion Nro." + sesion.NSesion;
        console.log("sesion", sesion);
        this.sesionSelected = { citaSesionID: sesion.CitaSesionID, sesionNro: sesion.NSesion, usuarioOldID: sesion.UsuarioID, citaID: this.data.citaID }
        this.isProgramarSesion = true;
        this.disabledDate = (current: Date): boolean => {
            return differenceInCalendarDays(new Date(sesion.FechaDisponible), current) > 0;
        };
    }

    cancelProgramacionSesion() {
        this.isProgramarSesion = false
    }

    async habilitarSigSesion() {
        debugger
        try {
            this._obvsService.toogleSpinner()
            const { Descripcion, Identificador, Resultado, Sesiones }: IApiResponse = await this._citaService.habilitarNuevaSesion({ citaID: this.data.citaID, fechaInicio: this.fechaSelectedHabilitacion, tomarMismoNroSesion: false });
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            if (Identificador == "success") {
                this._obvsService.toogleSpinner()
                await Swal.fire({
                    title: 'Se genero la siguiente sesion correctamente',
                    icon: 'success',
                    timer: 2500
                })
                this.obtenerSesionesAll();
                this.sigSesion = false
    
            }
        } catch (error) {
            this._obvsService.toogleSpinner()
        } finally {
            // this._obvsService.toogleSpinner()
        }
        // this.disabledDate  = (current: Date): boolean => {
        //     return differenceInCalendarDays(new Date(this.sesionSelected.fechaSesionAnterior), current) + 1 > 0;
        // };
        
    }

    async cancelarCita(sesion: any) {
        console.log(sesion)
        try {
            this.data.citaID
            let citaId = this.data.citaID;
            this.spinner.show();
            const { Identificador, Descripcion } = await this._citaService.cancelarCita({ citaID: citaId, puedeDevolverMitad: false });
            this.spinner.hide();
            if (Identificador == "success") {
                await Swal.fire({
                    icon: 'success',
                    text: Descripcion,
                    timer: 2500
                });
                // this.dialogRef.close(1);
                this.obtenerSesionesAll();
            } else {
                return await Swal.fire({
                    title: 'Alerta!',
                    icon: 'warning',
                    text: Descripcion,
                    timer: 2500
                });
                // }
    
            }
        } catch (error) {

        } finally {
            this.spinner.show();
        }
       
    }

    onChange2(event: any) {
        console.log("event", event)
        this.fechaSelectedHabilitacion = event
    }


    reprogramarSes(sesion: any) {
        this.sesionSelected = { citaSesionID: sesion.CitaSesionID, sesionNro: sesion.NSesion, usuarioOldID: sesion.UsuarioID }
        this.sesionNroSelected = "Sesion Nro." + sesion.NSesion;
        this.reprogramarSesion = true;
        this.disabledDate = (current: Date): boolean => {
            return differenceInCalendarDays(new Date(sesion.FechaDisponible), current) + 1 > 0;
        };
    }

    programarSesion(sesion: any) {
        console.log("sesion", sesion);
        this.sesionNroSelected = "Sesion Nro." + sesion.NSesion;
        this.sesionSelected = { citaSesionID: sesion.CitaSesionID, sesionNro: sesion.NSesion, usuarioOldID: sesion.UsuarioID, nroSesion: sesion.NSesion }
        this.isProgramarSesion = true;
    }

    async programarSesionClick(sesion: any) {
        console.log("Reprogramar sesion ")
        try {
            this.spinner.show();
            const resp = await this._citaService.asignarNuevaSesion({
                citaSesionID: this.sesionSelected.citaSesionID,
                servicioID: this.data.servicioID,
                citaID: this.data.citaID,
                usuarioID: this.formularioGroup.get('trabajador').value,
                consultorioID: this.consultorioIDCMin,
                nroSesion: this.sesionSelected.nroSesion,
                fechaHora: new Date(this.fechaSeleccionadaString + " " + this.horaSelected),
            })
            console.log(resp);
            this.spinner.hide();
            this.programado = true;
            if (resp.Identificador == "success") {
                await Swal.fire({
                    title: 'Se programo correctamente',
                    icon: 'success',
                    timer: 2500
                })
                this.obtenerSesionesAll();
                this.isProgramarSesion = false;
            }
        } catch (error) {
            this.spinner.hide();
            console.log("error", error)
            this.programado = false
        } finally{
            this.spinner.hide();
        }
    }

    async reproSesion() {
        console.log("Reprogramar sesion ")
        try {
            this.spinner.show();
            const resp = await this._citaService.reporgramarCitaWeb({
                citaSesionID: this.sesionSelected.citaSesionID,
                clienteID: this.data.clienteID,
                consultorioID: this.consultorioIDCMin,
                fechaHora: new Date(this.fechaSeleccionadaString + " " + this.horaSelected),
                oldUsuarioID: this.sesionSelected.usuarioOldID,
                servicioID: this.data.servicioID,
                usuarioID: this.formularioGroup.get('trabajador').value
            });
            console.log("resp", resp)
            this.spinner.hide();
            this.reprogramado = true
            if (resp.Identificador == "success") {
                await Swal.fire({
                    title: 'Se reprogramó correctamente',
                    icon: 'success',
                    timer: 2500
                })
                this.obtenerSesionesAll();
                this.reprogramarSesion = false;
            }
            // const resp = await this._citaService.reporgramarCitaWeb({citaSesionID , servicioID , clienteID, usuarioID , oldUsuarioID ,  fechaHora, consultorioID});
        } catch (error) {
            this.spinner.hide();
            console.log("error", error)
            this.reprogramado = false
        } finally {
            this.spinner.hide();
        }
    }

    // mostrarPenalidad(sesion: String) {
    //     console.log(sesion)
    //     this.agregarPenalidad = true;
    //     this.sesionSelected = "Sesion Nro." + sesion;
    //     this.formularioGroup.get('monto').setValue(0)
    // }

    // ocultarPenalidad() {
    //     this.agregarPenalidad = false;
    //     this.reprogramarSesion = false;
    // }

    // validarMonto(): boolean {
    //     console.log("valida monto", this.formularioGroup.get('monto').value)
    //     var residuo = this.formularioGroup.get('monto').value
    //     var monto = this.formularioGroup.get('monto').value
    //     console.log("monto%1", monto % 1)
    //     if (monto % 1 == 0) {
    //         return true
    //     }
    //     return false
    // }
    // registrarPenalidad() {
    //     console.log("validacion monto = ", this.validarMonto())
    //     const validacionContinuar = this.validarMonto();
    //     this.reprogramarSesion = validacionContinuar;
    // }
    async onChange(event: any) {
        try {
            this._obvsService.toogleSpinner()
            console.log("event", event)
            var march = moment(event);
            var temp = march.locale('es');
            this.fechaSeleccionadaString = temp.format('yyyy-MM-DD');

            console.log("", this.formularioGroup.get('trabajador').value)
            const idTrabajador = this.formularioGroup.get('trabajador').value
            const fecha = this.datePipe.transform(event, 'yyyy-MM-dd')
            const { Descripcion, Identificador, Resultado, Sesiones, Consultorios }: IApiResponse = await this._citaService.obtenerHorarioOcupadoReservaCitaWeb({ clienteId: this.data.clienteID, servicioID: this.data.servicioID, trabajadorId: idTrabajador, fecha: fecha })
            if (Identificador == "success") {
                console.log("Consultorio", Consultorios)
                Consultorios.forEach(element => {
                    this.consultorioCarga.push({ size: element.HorarioOcupado.length, ConsultorioID: element.ConsultorioID })
                    const position = this.consultorioCarga.length - 1;
                    if (position == 0) {
                        this.consultorioCargaMin = this.consultorioCarga[position].size;
                        this.consultorioIDCMin = this.consultorioCarga[position].ConsultorioID;
                        this.positionLista = position;
                        console.log("1era corrida", this.consultorioCargaMin, " - idConsultorio", this.consultorioIDCMin)
                    } else {
                        if (this.consultorioCargaMin > this.consultorioCarga[position].size) {
                            this.consultorioCargaMin = this.consultorioCarga[position].size
                            this.consultorioIDCMin = this.consultorioCarga[position].ConsultorioID;
                            this.positionLista = position;
                            console.log("corrida ", position, " - ", this.consultorioCargaMin, "idConsultorio", this.consultorioIDCMin)
                        }
                    }
                });
                console.log("consultorioCarga", this.consultorioCarga)
                const listaHorarios = [Consultorios[this.positionLista]]
                this.listaHorarioOcupado = listaHorarios;
                console.log("listaHorarios", listaHorarios)
                this.funAux();

            }
        } catch (error) {

        } finally {
            this._obvsService.toogleSpinner()
        }


    }

    async obtenerersonalxServicioID() {
        debugger
        try {
            this._obvsService.toogleSpinner()
            const { Descripcion, Identificador, Resultado, Personal }: IApiResponse = await this._citaService.obtenerPersonalPorServicio(this.data.servicioID);
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.listaTrabajadores = Personal
        } catch (error) {
            return __messageSnackBar(this._matSnackBar, error)
        } finally {
            this._obvsService.toogleSpinner()
        }
    }

    funAux() {
        if (this.listaHorarioOcupado.length === 0) {
            this.listaHorarioInicio = this.horario.generarHorasDia(7, 20);
        } else {
            this.listaHorarioInicio = this.horario.bloquearHorariosOcupados(this.listaHorarioOcupado, this.duracionServicio, this.fechaSeleccionadaString);

        }
        console.log("listaHorarioInicio", this.listaHorarioInicio)
    }

    horaInicioChange(event: any) {
        console.log("event Horas", event)
        console.log("event Horas", this.formularioGroup.get('horaInicio').value)
        const order = (event) ? +event : -1;
        if (order < 0) return this.formularioGroup.get('horaFin').setValue("");
        this.horaSelected = this.listaHorarioInicio[order].value
        //this.selectedHoraFin=this.listaHorarioInicio[10];
        const index = order + this.duracionServicio / 15;

        if (index == 53) return this.formularioGroup.get('horaFin').setValue("20:15")
        if (index == 54) return this.formularioGroup.get('horaFin').setValue("20:30")
        if (index == 55) return this.formularioGroup.get('horaFin').setValue("20:45")
        if (index == 56) return this.formularioGroup.get('horaFin').setValue("21:00")
        this.formularioGroup.get('horaFin').setValue(this.listaHorarioInicio[index].value)
    }


}