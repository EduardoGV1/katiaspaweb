import { NgModule } from "@angular/core";
import { CalidadComponent } from "./calidad/calidad.component";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { PagesModule } from "../pages.module";
import { CalidadAtencionRoutingModule } from "./calidadatencion.routing";


@NgModule({
    declarations:[
        CalidadComponent
    ],
    imports:[
        CommonModule,
        SharedModule,
        PagesModule,
        CalidadAtencionRoutingModule
    ]
})
export class CalidadAtencionModule{}