import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IApiResponse } from 'src/app/shared/models/response';
import { CitaService } from 'src/app/shared/services/cita.service';
import { __messageSnackBar, __messageSnackBarLeft } from 'src/app/utils/helpers';

@Component({
    selector: 'app-devolucion',
    templateUrl: './devolucion.component.html'
})

export class DevolucionComponent implements OnInit {

    formatterSoles = (value: number) => `S./ ${value}`;
    parserSoles = (value: string) => value.replace('S./ ', '');
    labelDevolucion = "REGISTRAR DEVOLUCION";
    buttonDisabled:boolean=false;


    formularioGroup: FormGroup
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private _citaService: CitaService,
        private dialogRef: MatDialogRef<DevolucionComponent>,
    ) {
        console.log("data", data)
        this.formularioGroup = new FormGroup({
            monto: new FormControl(0, [Validators.max(data.monto), Validators.required]),
            comentario: new FormControl(null, Validators.required)
        })
    }

    ngOnInit() {
        debugger
        if (this.data.DevolucionID > 0) {
            this.labelDevolucion="DETALLE DEVOLUCION"
            this.formularioGroup.get('monto').setValue(this.data.MontoDevolucion)
            this.formularioGroup.get('comentario').setValue(this.data.Comentario)
            this.buttonDisabled=true
        }
    }

    async registrarDevolucion() {
        if (this.formularioGroup.get('monto').value == null || this.formularioGroup.get('monto').value == undefined ||
            this.formularioGroup.get('comentario').value == null || this.formularioGroup.get('comentario').value == undefined
        ) {
            return __messageSnackBarLeft(this._matSnackBar, "Ingrese un monto y comnetario valido")
        }
        if (this.formularioGroup.get('monto').value > this.data.MontoServicio) {
            return __messageSnackBarLeft(this._matSnackBar, "El monto a devolver no puede ser mas que el pagado")
        }
        try {
            const { Identificador, Mensaje, Descripcion }: IApiResponse = await this._citaService.registrarDevolucion(
                {
                    pagoServicioID: this.data.PagoServicioID,
                    monto: this.formularioGroup.get('monto').value,
                    comentario: this.formularioGroup.get('comentario').value
                }
            )
            if (Identificador == "success") {
                __messageSnackBar(this._matSnackBar, Descripcion)
                this.dialogRef.close({ data: true });
            }
            if (Identificador == "error") {
                return __messageSnackBarLeft(this._matSnackBar, Descripcion)
            }

        } catch (error) {
            return __messageSnackBarLeft(this._matSnackBar, error)
        }
    }
    close() {
        this.dialogRef.close({ data: false });
    }
}