import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-respuesta-encuesta',
    templateUrl: 'respuesta-encuesta.component.html'
})

export class RespuestaEncuestaComponent implements OnInit {

    @Input() respuestaArr = [];
    @Input() tipo: number = 0;
    totalMarcas = 0;
    constructor() { }
    
    ngOnInit() { 
        this.respuestaArr.sort( (el , next) => {
            return el.OrdenRespuesta - next.OrdenRespuesta
        })
        this.respuestaArr.forEach( (el) => this.totalMarcas += el.Cantidad)
    }
}