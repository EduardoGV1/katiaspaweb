import { DatePipe } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { IApiResponse } from "src/app/shared/models/response";
import { ServicioService } from "src/app/shared/services/servicio.service";
import { __messageSnackBar } from "src/app/utils/helpers";
import { AgregarClienteComponent } from "./agregar-clientes/agregar-clientes.component";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { ObvsService } from "src/app/shared/services/obvs.service";

@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

    clientesArr = [];
    clientesForm: FormGroup;
    fechaInicio: Date;
    fechaFin: Date;
    paginatorLength = 100;
    paginatorPageSize = 10;
    paginatorTotalRecords=0;
    // paginatorPageSizeOptions: number[] = [5, 10, 25, 100];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns: string[] = ['Nombres','Apellidos','Correo','Genero','Fecha Registro','Acción'];
    pageEvent: PageEvent;


    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar: MatSnackBar,
        private router: Router,
        public dialog: MatDialog,
        public _obvsService: ObvsService,
    ) {
        this.crearClientesForm()
    }

    async ngOnInit() {
        await this.listarClientes(1,this.paginatorPageSize)
    }
    async dev(event:any){
        console.log("event",event)
        await this.listarClientes(event.pageIndex*this.paginatorPageSize,this.paginatorPageSize)

    }

    crearClientesForm() {
        this.clientesForm = new FormGroup({
            genero: new FormControl(""),
            apellidos: new FormControl(null, [Validators.required]),
            nombres: new FormControl(null, [Validators.required]),
            fecha: new FormControl(null, [Validators.required]),
        })
    }

    limpiarFiltro(){
        this.crearClientesForm();
    }

    async listarClientes(start:number,length:number) {
        this._obvsService.toogleSpinner()
        const datePipe = new DatePipe('en-US');
        try {
            const {
                Identificador,
                Descripcion,
                Resultado,
                Total
            }: IApiResponse = await this._servicioService.listarClientesPaginado({
                genero: (this.clientesForm.value.genero == null || this.clientesForm.value.genero == "") ? null : JSON.parse(this.clientesForm.value.genero),
                apellidos: (this.clientesForm.value.apellidos == null) ? null : this.clientesForm.value.apellidos,
                nombres: (this.clientesForm.value.nombres == null) ? null : this.clientesForm.value.nombres,
                fechaInicio: datePipe.transform(this.fechaInicio, 'yyyy-MM-dd'),
                fechaFin: datePipe.transform(this.fechaFin, 'yyyy-MM-dd'),
                start: start,
                length: length
            })
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.clientesArr = Resultado
            this.paginatorTotalRecords=Total
        }
        catch (error) {
            console.log(error)
        } finally{
            this._obvsService.toogleSpinner()
        }
    }

    listarClientexID(ClienteID: number) {
        this.router.navigate(['/Persons/VerCliente/' + ClienteID])
    }
    agregarCliente() {
        const dialogRef = this.dialog.open(AgregarClienteComponent, { disableClose: true })
        dialogRef.afterClosed().subscribe(() => {
            if (dialogRef.componentInstance.changeData) this.ngOnInit()
        })
    }
    async buscarCliente() {
        const datePipe = new DatePipe('en-US');
        try {
            const {
                Identificador,
                Descripcion,
                Resultado
            }: IApiResponse = await this._servicioService.listarClientesPaginado({
                genero: (this.clientesForm.value.genero == null || this.clientesForm.value.genero == "") ? null : JSON.parse(this.clientesForm.value.genero),
                apellidos: (this.clientesForm.value.apellidos == null) ? null : this.clientesForm.value.apellidos,
                nombres: (this.clientesForm.value.nombres == null) ? null : this.clientesForm.value.nombres,
                fechaInicio: datePipe.transform(this.fechaInicio, 'yyyy-MM-dd'),
                fechaFin: datePipe.transform(this.fechaFin, 'yyyy-MM-dd'),
                start: 1,
                length: this.paginatorPageSize
            })
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.clientesArr = Resultado
        }
        catch (error) {
            console.log(error)
        }
    }
    changeRangePicker(range: Date[]) {
        console.log("evento:", range);
        this.fechaInicio = null;
        this.fechaFin = null;
        (range.length == 2) ? (this.fechaInicio = range[0], this.fechaFin = range[1]) : 0;
        (range.length == 1) ? (this.fechaInicio = range[0], this.fechaFin = null) : 0;
    }
}