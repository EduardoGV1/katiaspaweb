import { Component, Inject, Input, OnInit } from '@angular/core';
import { IApiResponse } from '../../../../../shared/models/response';
import { __messageSnackBar } from '../../../../../utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PersonsService } from 'src/app/shared/services/persons.service';
import { FormArray, FormControl } from '@angular/forms';
import { ObvsService } from '../../../../../shared/services/obvs.service';

@Component({
  selector: 'app-agregar-servicio-personal',
  templateUrl: './agregar-servicio-modal.component.html'
})
export class AgregarServicioModalComponent implements OnInit {

  serviciosArr: IServicio[] = []

  constructor(
    private _personsService: PersonsService,
    private _obvsService: ObvsService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AgregarServicioModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    console.log(data)
  }

  async ngOnInit() {
    this._obvsService.toogleSpinner()
    await this.listarServiciosAsignadosTrabajador();
    await this.listarServiciosDisponiblesTrabajador();
    this._obvsService.toogleSpinner()
    console.log(this.serviciosArr)
  }

  async listarServiciosAsignadosTrabajador() {
    try {
      const {
        Descripcion,
        Identificador,
        Resultado
      }: IApiResponse = await this._personsService.listarServiciosAsignadosTrabajador({
        usuarioID: this.data
      })
      if (Identificador == "error") {
        return __messageSnackBar(this._matSnackBar, Descripcion)
      }
      Resultado.forEach(service => {
        this.serviciosArr.push({
          servicioID: service.ServicioID,
          nombre: service.Nombre,
          asignado: true,
          checked: true
        })
        console.log(service)
      }
      )
    } catch (error) {
      console.log(error)
    }
  }

  async listarServiciosDisponiblesTrabajador() {
    try {
      const {
        Descripcion,
        Identificador,
        Resultado
      }: IApiResponse = await this._personsService.listarServiciosDisponiblesTrabajador({
        usuarioID: this.data
      })
      if (Identificador == "error") {
        return __messageSnackBar(this._matSnackBar, Descripcion)
      }
      Resultado.forEach(service => this.serviciosArr.push({
        servicioID: service.ServicioID,
        nombre: service.Nombre,
        asignado: false,
        checked: false
      })
      )
    } catch (error) {
      console.log(error)
    }
  }

  changeValue(evt, index) {
    const checked = evt.target.checked;
    this.serviciosArr[index].checked = checked
    console.log(evt)
  }

  async updateService() {
    try {
      this._obvsService.toogleSpinner()
      this.serviciosArr.forEach(async (element) => {
        if (!element.asignado && element.checked)
          await this._personsService.asignarServicio({
            servicioID: element.servicioID,
            usuarioID: this.data
          })
        if (element.asignado && !element.checked)
          await this._personsService.quitarServicio({
            servicioID: element.servicioID,
            usuarioID: this.data
          })
      })
      __messageSnackBar(this._matSnackBar, "Se actualizó exitosamente")
      this.dialogRef.close()
    } catch (err) {
      console.log(err)
    } finally {
      this._obvsService.toogleSpinner()
    }
  }
}

export interface IServicio {
  servicioID: number;
  nombre: string;
  asignado: boolean;
  checked: boolean;
}