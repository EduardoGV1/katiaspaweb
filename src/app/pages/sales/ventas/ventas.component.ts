import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../../shared/services/sales.service';
// import * as moment from 'moment/moment';
import { DatePipe, formatDate } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DatePickerService } from 'ng-zorro-antd/date-picker/date-picker.service';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';

import * as moment from 'moment';
import {Moment} from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';

//const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-ventas-component',
    templateUrl: './ventas.component.html',
    // providers:[
    //     {
    //         provide: DateAdapter,
    //         useClass: MomentDateAdapter,
    //         deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    //       },
    // ]
})

export class VentasComponent implements OnInit {

    //FILTROS
    date = new FormControl(new Date());
    dateHoy=new Date();

    dateInicioActual = new Date(this.dateHoy.getFullYear(), this.dateHoy.getMonth(), 1);
    dateFinActual = new Date(this.dateHoy.getFullYear(), this.dateHoy.getMonth() + 1, 0);
    rangoDateServiciosMasVendidos:Date[]=[this.dateInicioActual,this.dateFinActual];

    anioResumenVentasAnual=this.dateHoy;
    

    ventasArr = [];
    ventasArrTotal = [];
    fechainicio = moment().format('yyyy-MM-dd')
    montoTotal = 0;
    serviciosCantidad = 0;
    montoTotalTotal = 0;
    serviciosCantidadTotal = 0;
    fechaInicio: Date;
    fechaFin: Date;
    filtrosForm: FormGroup; 
    filtroIsActive: Boolean = true;
    listaFechasPagosxMes: any[] = [];
    listaServicioxRango: any[] = [];
    listaPagosxYear: any[] = [];
    displayedColumns: string[] = ['Fecha', 'Monto','Devuelto','Total'];
    displayedColumnsServicios: string[] = ['Servicio', 'Monto','Devuelto','Total'];
    displayedColumnsAnios: string[] = ['Mes', 'Total','Devuelto','Total2'];

    //Graficas
    saleDataCard: any = []
    // single: any[];
    view: any[] = [700, 400];

    // options
    showXAxis: boolean = true;
    showYAxis: boolean = true;
    gradient: boolean = false;
    showLegend: boolean = true;
    showXAxisLabel: boolean = true;
    yAxisLabel: string = 'DIAS';
    showYAxisLabel: boolean = true;
    xAxisLabel: string = 'MONTO';
    listaValoresBarHorizontal: any[] = [];
    totalDiario:number=0;
    totalDiarioDevuelto:number=0;

    //graficas 2
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    legendPosition: string = 'below';
    listaValoresPieChart: any[] = [];
    totalxServicio:number=0;
    totalxServicioDevuelto:number=0;

    //graficas 3
    listaValoresLinearItem: any[] = []
    listaValoresLinearItemDevuelto: any[] = []
    listaValoresLinear: any[] = [];
    timeline: boolean = true;
    showGridLines: boolean = true
    yAxisLabel3: string = 'MONTOS';
    showYAxisLabel3: boolean = true;
    xAxisLabel3: string = 'MESES';
    totalxYear:number=0;

    colorScheme = {
        domain: ['#E61212', '#E67212', '#E6B912', '#D6E612', '#8CE612', '#12E659', '#12E6BF', '#12B6E6', '#1259E6', '#2812E6', '#8612E6', '#E612C3']
    };

    //Tab Paginado Reporte
    pagina:number=1;
    cantidad:number=10;
    listaReportePaginado: any[] = [];
    displayedColumnsReportes: string[] = ['FechaInicio','FechaFin', 'Total','Detalle'];

    listaServiciosReporte:any[]=[];
    displayedColumnsReportesporServicio: string[] = ['ServicioNombre','Total','MontoDevuelto','Total2'];
    totalReprotexServicio:number=0;
    totalReprotexServicioDevuelto:number=0;


    constructor(
        private _ventasService: SalesService,
        private _matSnackBar: MatSnackBar,
        public datePipe: DatePipe,
    ) {
        this.crearFiltroForm();
        console.log("this.date",this.date)
        // Object.assign(this, { this.single });
        console.log("dateInicioActual",this.dateInicioActual)
        console.log("dateFinActual",this.dateFinActual)
    }

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }
    async ngOnInit() {
        this.listarFechasPagosxMes(this.dateHoy.getFullYear(),this.dateHoy.getMonth()+1);
        this.obtenerServiciosVentaxRangoFecha(this.dateInicioActual,this.dateFinActual);
        this.listarFechasPagosxYear(this.dateHoy.getFullYear());
        this.listarReportes();

    }

    crearFiltroForm() {
        this.filtrosForm = new FormGroup({
            fechas: new FormControl(null)
        })
    }

    async listarFechasPagosxMes(year:number, month:number) {
        try {
            debugger
            // let month = this.dateHoy.getMonth()+1;
            // let year = this.dateHoy.getFullYear();
            const { Identificador, Ventas } = await this._ventasService.listarFechasPagosxMes({ year: year, month: month })
            console.log("Ventas", Ventas)
            this.listaFechasPagosxMes=[]
            this.totalDiario=0;
            this.totalDiarioDevuelto=0;
            this.listaValoresBarHorizontal=[];

            this.listaFechasPagosxMes = Ventas
            this.listaFechasPagosxMes.forEach(item => {
                this.listaValoresBarHorizontal.push(
                    {
                        "name": item.FechaPago,
                        "value": item.Total
                    }
                )
                this.totalDiario=this.totalDiario+item.Total;
                this.totalDiarioDevuelto=this.totalDiarioDevuelto+item.TotalDevuelto;
            });
        } catch (error) {
            console.log("Error", error)
        }
    }

    changeResumenVentasDiarias(fechaFiltro: Date){
        console.log("evento:", fechaFiltro);
        this.dateHoy=fechaFiltro;

    }

    async buscarResumenVentasDiarias(){
        if (this.dateHoy==null || this.dateHoy==undefined) {
            return __messageSnackBar(this._matSnackBar, "Se debe ingresar una fecha valida")
        }
         await this.listarFechasPagosxMes(this.dateHoy.getFullYear(),this.dateHoy.getMonth()+1);
    }

    changeRangoServiciosMasVendidos(range: Date[]) {
        console.log("evento:", range);
        this.fechaInicio = null;
        this.fechaFin = null;
        (range.length == 2) ? (this.dateInicioActual = range[0], this.dateFinActual = range[1]) : 0;
        (range.length == 1) ? (this.dateInicioActual = range[0], this.dateFinActual = null) : 0;
        console.log(range.length)
        this.filtroIsActive = (range.length == 2) ? false : true;
        this.dateInicioActual,this.dateFinActual
    }

    async buscarServiciosMasVendidos(){
        if (this.dateInicioActual==null || this.dateInicioActual==undefined || this.dateFinActual==null || this.dateFinActual==undefined) {
            return __messageSnackBar(this._matSnackBar, "Se debe ingresar un rango de fechas validas")
        }
        await this.obtenerServiciosVentaxRangoFecha(this.dateInicioActual,this.dateFinActual);
    }

    async obtenerServiciosVentaxRangoFecha(fechaInicio:Date, fechaFin:Date) {
        if (fechaInicio==null || fechaInicio==undefined || fechaFin==null || fechaFin==undefined) {
            return __messageSnackBar(this._matSnackBar, "Se debe ingresar un rango de fechas validas")
        }
        const fechaIni = this.datePipe.transform(fechaInicio, 'yyyy-MM-dd')
        const fechaFi = this.datePipe.transform(fechaFin, 'yyyy-MM-dd')
        try {
            const { Identificador, Ventas } = await this._ventasService.obtenerServiciosVentaxRangoFecha({ fechaInicio: fechaIni, fechaFin: fechaFi })
            console.log("Ventas", Ventas)
            this.totalxServicio=0;
            this.totalxServicioDevuelto=0;
            this.listaServicioxRango=[];
            this.listaServicioxRango = Ventas
            this.listaServicioxRango.forEach(item => {
                this.listaValoresPieChart.push(
                    {
                        "name": item.ServicioNombre,
                        "value": item.Total
                    }
                )
                this.totalxServicio=this.totalxServicio+item.Total
                this.totalxServicioDevuelto=this.totalxServicioDevuelto+item.TotalDevuelto
            });
        } catch (error) {
            console.log("Error", error)
        }
    }

    async listarFechasPagosxYear(year:number) {
        try {
            const { Identificador, Ventas } = await this._ventasService.listarFechasPagosxYear({ year: year })
            console.log("Ventas", Ventas)
            this.listaPagosxYear=[];
            this.listaValoresLinearItemDevuelto=[]
            this.totalxYear=0;
            this.listaValoresLinear=[];
            this.listaValoresLinearItem=[];
            this.listaPagosxYear = Ventas
            this.listaPagosxYear.forEach(item => {
                this.listaValoresLinearItem.push(
                    {
                        "value": item.Total,
                        "name": item.MesNombre
                    }
                )
                this.listaValoresLinearItemDevuelto.push(
                    {
                        "value": item.DevolucionTotal,
                        "name": item.MesNombre
                    }
                )
                this.totalxYear=this.totalxYear+item.Total
            });
            this.listaValoresLinear.push(
                {
                    "name": "GANANCIA",
                    "series": this.listaValoresLinearItem
                }
            );
            this.listaValoresLinear.push(
                {
                    "name": "DEVOLUCION",
                    "series": this.listaValoresLinearItemDevuelto
                }
            );
            console.log("eeee", this.listaValoresLinear)

        } catch (error) {
            console.log("Error", error)
        }
    }

    changeResumenVentasAnual(anio:any){
        this.anioResumenVentasAnual=anio
    }

    async buscarResumenVentasAnual(){
        if (this.anioResumenVentasAnual==null || this.anioResumenVentasAnual==undefined) {
            return __messageSnackBar(this._matSnackBar, "Se debe ingresar una fecha valida")
        }
        await this.listarFechasPagosxYear(this.anioResumenVentasAnual.getFullYear());
    }

    async listarReportes(){
        try {
            const { Identificador, Mensaje,Descripcion,Reportes } = await this._ventasService.listarReportePaginado({ pagina: this.pagina, cantidad:this.cantidad })
            console.table(Reportes)
            this.listaReportePaginado=Reportes;
        } catch (error) {
            
        }
    }

    async listarVentas(pagina: number = 1) {
        try {
            const {
                Personal
            } = await this._ventasService.obtenerVentasPorRango({
                fechaInicio: moment(new Date()).format('YYYY-MM-DD'),
                fechaFin: moment(new Date()).format('YYYY-MM-DD'),
                pagina: pagina,
                cantidad: 10
            })
            console.log(moment(new Date()).format('YYYY-MM-DD'))
            this.ventasArr = Personal
            var sum = 0;
            for (let index = 0; index < Personal.length; index++) {
                sum = Personal[index].CantidadPago + sum;
            }
            this.montoTotal = sum;
            this.serviciosCantidad = Personal.length;

        } catch (error) {
            console.log(error)
        }
    }
    async listarVentasTotal(pagina: number = 1) {
        const fechaHoy = moment(new Date()).format('YYYY-MM-DD')
        try {
            const {
                Personal
            } = await this._ventasService.obtenerVentasPorRango({
                fechaInicio: '2020-01-01',
                fechaFin: fechaHoy,
                pagina: pagina,
                cantidad: 10
            })
            this.ventasArrTotal = Personal
            var sum = 0;
            for (let index = 0; index < Personal.length; index++)
                sum = Personal[index].CantidadPago + sum;

            this.montoTotalTotal = sum;
            this.serviciosCantidadTotal = Personal.length;

            console.log(Personal)
        } catch (error) {
            console.log(error)
        }

    }
    async filtrarVentasTotalesByFilter() {
        debugger
        if (this.fechaInicio == null || this.fechaFin == null) {
            __messageSnackBar(this._matSnackBar, "Seleccionar fechas validas");
        }
        try {
            const date = new Date();
            const datePipe = new DatePipe('en-US');
            //const formattedDate = datePipe.transform(date,'yyyy-MM-dd');
            console.log(datePipe.transform(this.fechaInicio, 'yyyy-MM-dd'))
            const {
                Personal
            } = await this._ventasService.obtenerVentasPorRango({
                fechaInicio: datePipe.transform(this.fechaInicio, 'yyyy-MM-dd'),
                fechaFin: datePipe.transform(this.fechaFin, 'yyyy-MM-dd'),
                pagina: 1,
                cantidad: 10
            })
            this.ventasArrTotal = Personal
            var sum = 0;
            for (let index = 0; index < Personal.length; index++)
                sum = Personal[index].CantidadPago + sum;

            this.montoTotalTotal = sum;
            this.serviciosCantidadTotal = Personal.length;
        } catch (error) {

        }
        console.log("date", this.filtrosForm)
    }

    showDetail(listaServicios:any){
        if (listaServicios) {
            this.totalReprotexServicio=0;
            this.listaServiciosReporte=listaServicios;
            this.listaServiciosReporte.forEach(itemServicio => {
                this.totalReprotexServicio=itemServicio.Total+this.totalReprotexServicio;
                this.totalReprotexServicioDevuelto=itemServicio.TotalDevuelto+this.totalReprotexServicioDevuelto
            });
        }
        console.log(this.listaServiciosReporte)
    }

    onOk(event: Date[]) {
        console.log(event)
    }
    change(range: Date[]) {
        console.log("evento:", range);
        this.fechaInicio = null;
        this.fechaFin = null;
        (range.length == 2) ? (this.fechaInicio = range[0], this.fechaFin = range[1]) : 0;
        (range.length == 1) ? (this.fechaInicio = range[0], this.fechaFin = null) : 0;
        console.log(range.length)
        this.filtroIsActive = (range.length == 2) ? false : true;
    }

    chosenYearHandler(normalizedYear: Moment) {
        console.log("normalizedYear",normalizedYear)
        const ctrlValue = this.date.value;
        ctrlValue.year(new Date(normalizedYear.toString()).getFullYear);
        this.date.setValue(ctrlValue);
      }
    
      chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        console.log("normalizedMonth",normalizedMonth)
        const ctrlValue = this.date.value;
        ctrlValue.month(new Date(normalizedMonth.toString()).getMonth);
        this.date.setValue(ctrlValue);
        datepicker.close();
      }


}