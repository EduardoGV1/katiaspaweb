import { formatDate, getLocaleDateTimeFormat } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { PersonsService } from "src/app/shared/services/persons.service";
import { IApiResponse, IRoles } from '../../../../shared/models/response';
import * as moment from 'moment'
import { __messageSnackBar } from "src/app/utils/helpers";
import { RolesService } from "src/app/shared/services/roles.Service";
import { ViewChild } from "@angular/core";
import { NzDatePickerComponent } from "ng-zorro-antd";
const ToDay=moment(new Date()).add(-18,'years').toDate()

@Component({
    selector: 'app-agregar-personal',
    templateUrl: './agregar-personal.component.html'
})
export class AgregarPersonalComponent implements OnInit {
    formularioGroup: FormGroup
    ategoriasArr = []
    rolesArr=[]
    changeData = false
    fotoTrabajador: any = ""
    @ViewChild('endDatePicker') endDatePicker!: any;
    
    disabledEndDate = (startValue: Date): boolean => {
        if (!startValue || !ToDay) {
          return false;
        }
        return startValue.getTime() > ToDay.getTime();
      };
      

    constructor(
        private _personsService: PersonsService,
        private _matSnackBar: MatSnackBar,
        private _activeRoute: ActivatedRoute,
        public dialogRef: MatDialogRef<AgregarPersonalComponent>,
        private _rolesService:RolesService
    ) {
        this.formularioGroup = new FormGroup({
            nombre: new FormControl(null, [Validators.required]),
            apellido: new FormControl(null, [Validators.required]),
            dni: new FormControl(null, [Validators.required,Validators.minLength(6),Validators.maxLength(9)]),
            telefono: new FormControl(null, [Validators.required,Validators.minLength(8),Validators.maxLength(10)]),
            correo: new FormControl(null, [Validators.required,Validators.email]),
            genero: new FormControl("", [Validators.required]),
            rolID: new FormControl("", [Validators.required]),
            direccion: new FormControl(null, [Validators.required]),
            fecha:new FormControl(null,[Validators.required])
        })
    }

    markFormGroupTouched(formGroup: FormGroup) {
        Object.values(formGroup.controls).forEach(control => {
          control.markAsTouched();
      
          if (control instanceof FormGroup) {
            this.markFormGroupTouched(control);
          }
        });
    }

    async ngOnInit() {
        console.log(moment(new Date()).format('YYYY-MM-DD'))
        try {
            const{
                Mensaje,
                Identificador,
                Descripcion,
                Roles
            }:IRoles = await this._rolesService.listarRoles({
                Start:1,
                Length:20
            })
            this.rolesArr=Roles
            console.log(this.rolesArr)
        } catch (error) {
            console.log(error)
        }

    }

    async registrarTrabajador() {
        const a=formatDate(this.formularioGroup.get('fecha').value,'YYYY-MM-DD','en-US')
        console.log(this.formularioGroup.get('fecha').value)
        console.log(a)
        if (this.formularioGroup.valid) {
            console.log("valido")
        } else {
            console.log("no valido")
            return this.markFormGroupTouched(this.formularioGroup)
        }
        // if(this.formularioGroup.invalid){
        //     return Object.values(this.formularioGroup.controls).forEach(control => {
        //         control.markAsDirty();
        //       })
        // }
        try {
            const _genero = (this.formularioGroup.get('genero').value===1);
            this.formularioGroup.disable();
            const {
                Mensaje,
                Identificador,
                Descripcion
             }: IApiResponse = await this._personsService.agregarTrabajador({
                nombres: this.formularioGroup.get('nombre').value,
                apellidos: this.formularioGroup.get('apellido').value,
                dni: Number(this.formularioGroup.get('dni').value),
                telefono: Number(this.formularioGroup.get('telefono').value),
                correo: this.formularioGroup.get('correo').value,
                fechaRegistroString: moment(new Date()).format('YYYY-MM-DD'),
                genero: _genero,
                rol: {
                    rolID: Number(this.formularioGroup.get('rolID').value),
                },
                fechaNacimiento:this.formularioGroup.get('fecha').value,
                direccion:this.formularioGroup.get('direccion').value,
                foto: this.fotoTrabajador.split(',')[1]
            }
            )
            if(Identificador === "success") {
                this.dialogRef.close();
                this.changeData=true
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log(error);
        } finally {
            this.formularioGroup.enable();
        }
    }

    addFotos() {
        const input: HTMLInputElement = document.querySelector('#fileFotos');
        this.fileTo64Bits(input.files);
        console.log(input.files);
    }
    fileTo64Bits(imagen: FileList) {
        const foto = imagen[0]
        if (foto.type.startsWith("image")) {
            const reader = new FileReader();
            reader.readAsDataURL(foto);
            reader.onloadend = () => {
                const newFile: any = reader.result;
                this.fotoTrabajador = newFile
                //.split(',')[1];


                console.log(newFile);
            }
        } else {
            console.log("Error")
        }
    }

    deleteFoto() {
        this.fotoTrabajador = null;

    }
}