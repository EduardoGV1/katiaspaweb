import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages.component';
import { RolComponent } from './rol/rol.component';

const routes:Routes=[
    {
        path     : '',
        component: PagesComponent,
        children:[
            {
                path: '',
                component: RolComponent,
                data:{breadcrumb: "Gestion de Roles"}
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RolesRoutingModule { }