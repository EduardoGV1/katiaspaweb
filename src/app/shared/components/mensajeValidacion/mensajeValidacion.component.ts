import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from 'preact/compat';
@Component({
    selector:'mensajeValidacion',
    templateUrl:'./mensajeValidacion.component.html',
    providers:[
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting:forwardRef(()=> MensajeValidacionComponent),
            multi:true
        }
    ]
})
export class MensajeValidacionComponent {
    @Input() mensaje:String;
    //validate:string='';
     constructor(){}
    // ngOnInit(): void {
    // }
    // writeValue(value: any): void {
    //     console.log("value"+value)
    //     this.validate=value
    // }
    // registerOnChange(fn: any): void {
    //     throw new Error('Method not implemented.');
    // }
    // registerOnTouched(fn: any): void {
    //     throw new Error('Method not implemented.');
    // }
    // setDisabledState?(isDisabled: boolean): void {
    //     throw new Error('Method not implemented.');
    // }
}