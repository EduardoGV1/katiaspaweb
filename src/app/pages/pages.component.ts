import { Component} from '@angular/core';
import { getModulos } from '../utils/storage';
import { IModules } from '../shared/models/response';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent {

  modulos: Array<IModules>

  constructor() { 
    this.modulos = getModulos()
  }

}
