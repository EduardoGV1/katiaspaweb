import { MatSnackBar } from "@angular/material/snack-bar";
import * as moment from 'moment/moment';

export function __messageSnackBar(snackBar: MatSnackBar, message: any, duration?: number): void {
    if (typeof(message) == 'undefined') message = "Ocurrió un error."
    if (typeof(duration) == 'undefined') duration = 7000;
    snackBar.open(message, 'Cerrar', {
        duration          : duration,
        horizontalPosition: "end",
        verticalPosition  : "top"
    });
}

export function __messageSnackBarLeft(snackBar: MatSnackBar, message: any, duration?: number): void {
    if (typeof(message) == 'undefined') message = "Ocurrió un error."
    if (typeof(duration) == 'undefined') duration = 7000;
    snackBar.open(message, 'Cerrar', {
        duration          : duration,
        horizontalPosition: "start",
        verticalPosition  : "top"
    });
}

export function _orderRangeDate(dateRange: Array<Date>) {
    let fechaIni, fechaFin;
    if(moment(dateRange[0]).isBefore(dateRange[1])) {
        fechaIni = moment(dateRange[0]).format('yyyy-MM-DD');
        fechaFin = moment(dateRange[1]).format('yyyy-MM-DD');
    }
    else {
        fechaIni = moment(dateRange[1]).format('yyyy-MM-DD');
        fechaFin = moment(dateRange[0]).format('yyyy-MM-DD');
    }
    return [fechaIni,fechaFin]
}

export function __cloneArr(obj) {
    if (obj === null || typeof obj !== 'object') return obj;
    var temp = obj.constructor()
    for (var key in obj) temp[key] = __cloneArr(obj[key])
    return temp;
}

export function __clearReference(obj) {
    return JSON.parse(JSON.stringify(obj))
}

export function trimLowerWord(text: string): string {
    if (!text) return '';
    return text.trim().toLowerCase()
}