export interface IApiResponse {
    Mensaje      : string;
    Descripcion  : string;
    Identificador: string;
    Resultado?   : any;
    Citas?       : any
    CitasInfo?    : any;
    Sesiones?:any;
    Personal?:any;
    Consultorios?:any;
    Paginado?:any;
    Estados?:any;
    Total?:any;
}

export interface ILoginModel extends IApiResponse {
    Menu    : any;
    Personal: any;
    Token:any;
}

export interface ICategoriasModel extends IApiResponse {
    Accion             : number;
    Descripcion        : string;
    TotalServicios     : number;
    CategoriaServicioID: number;
}

export interface IModules {
    Ruta     : string;
    Tipo     : "Route" | "DropDownList";
    Icono?   : string;
    Titulo   : string;
    Children?: Array<IModules>;
}

export interface INewServicio {
    servicioID?: number;
    nombre     : string;
    descripcion: string;
    foto       : string;
    isActivo   : boolean;
    categoriaServicio: {
      categoriaServicioID: number;
    },
    detalleServicio: {
      precio          : number;
      duracion        : number;
      genero          : string;
      tiempoDescanso  : number;
      nroSesionesAprox: number;
      pagoCuota       : boolean;
    }
}
export interface IDashboard{
    Mensaje      : string;
    Descripcion  : string;
    Identificador: string;
    CitasPorMesTotal:any;
    CitasPorYearTotal:any;
    VentasTotal:any;
    VentasMensualesTotal:any;
    CitasTotal:any;
    SesionesTotal:any;
    ClientesTotal:any;
    SesionesPendientesTotal:any;
    ServiciosPorMesTotal:any;
}
export interface IRoles{
    Mensaje      : string;
    Descripcion  : string;
    Identificador: string;
    Roles:any;
}