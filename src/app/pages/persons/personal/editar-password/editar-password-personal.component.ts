import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { PersonsService } from "src/app/shared/services/persons.service";
import { __messageSnackBar } from "src/app/utils/helpers";
import { IApiResponse } from '../../../../shared/models/response';
import { DialogData } from "../personal.component";

@Component({
    selector: 'app-editar-password-personal',
    templateUrl: './editar-password-personal.component.html'
})


export class EditarPasswordComponent implements OnInit{
    formularioGroup: FormGroup
    flag=false;
    changeData = false
    // usuario="asd"
    // password="asd"
    constructor(
        private _matSnackBar: MatSnackBar,
        private _personsService: PersonsService,
        public dialogRef:MatDialogRef<EditarPasswordComponent>,
        @Inject(MAT_DIALOG_DATA) public data:DialogData
    ){
        this.formularioGroup = new FormGroup({
            nombre: new FormControl(data.nombre+" "+data.nombre),
            password: new FormControl(null,Validators.required)
        })
        
    }
    
    async ngOnInit(){
        try {
        } catch (error) {
            
        }

    }
    async editarPassword(){
        try {
            this.formularioGroup.disable()
            const{
                Mensaje,
                Identificador,
                Descripcion
            }:IApiResponse = await this._personsService.cambiarPasswordTrabajador({
                usuarioID:(Number)(this.data.usuarioID),
                password: this.formularioGroup.get('password').value
            })
            if(Identificador === "success") {
                this.dialogRef.close();
                this.changeData=true
            }
        } catch (error) {
            console.log(error)
        } finally {
            this.formularioGroup.enable();
        }
    }
    async enviarPassword(){
        try {
            console.log(this.data.usuarioID);
            this.formularioGroup.disable();
            const{
                Mensaje,
                Identificador,
                Descripcion
            }:IApiResponse = await this._personsService.enviarPasswordTrabajador({
                usuarioID: (Number)(this.data.usuarioID)

            })
            if(Identificador==="success"){
                this.dialogRef.close();
                this.changeData=true;
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log(error)
        } finally{
            this.formularioGroup.enable()
        }

    }
    mostrarForm(){
        this.flag=!this.flag;
    }
}