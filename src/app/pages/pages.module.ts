import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Shared Module
import { RouterModule } from '@angular/router';
// Component
import { PagesComponent } from './pages.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FooterComponent } from '../shared/footer/footer.component';
import { HeaderComponent } from '../shared/header/header.component';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { Dias2LetrasPipe } from './pipes/dias2Letras.pipe';

@NgModule({
  declarations: [
    PagesComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    Dias2LetrasPipe
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    RouterModule,
    FeatherModule.pick(allIcons)
  ],

})
export class PagesModule { }
