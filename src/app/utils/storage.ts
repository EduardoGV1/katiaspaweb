const TOKEN_KEY     = 'OAuthToken';
const PERMISO_KEY   = 'Permisos'
const USER_DATA_KEY = 'UserData'
const MODULOS_KEY   = 'Modulos'
export function removeToken() {
    window.localStorage.removeItem(TOKEN_KEY);
}

export function removeSession() {
    window.localStorage.clear();
}

export function saveToken(token: string) {
    removeToken()
    window.localStorage.setItem(TOKEN_KEY, token);
}

export function getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
}

export function getRol(): { RolID: number; Descripcion: string;} {
    return JSON.parse(localStorage.getItem(USER_DATA_KEY)).Rol;
}

export function removePermisos() {
    window.localStorage.removeItem(PERMISO_KEY)
}

export function savePermisos(token: string) {
    removePermisos()
    window.localStorage.setItem(PERMISO_KEY, JSON.stringify(token));
}

export function getPermisos(): string {
    return localStorage.getItem(JSON.parse(PERMISO_KEY));
}

export function removeUserData() {
    window.localStorage.removeItem(USER_DATA_KEY)
}

export function saveUserData(userData) {
    removeUserData()
    window.localStorage.setItem(USER_DATA_KEY, JSON.stringify(userData));
}

export function getUserData(): string {
    return JSON.parse(localStorage.getItem((USER_DATA_KEY)));
}

export function saveModulos(token) {
    removeModulos()
    window.localStorage.setItem(MODULOS_KEY, JSON.stringify(token));
}

export function removeModulos() {
    window.localStorage.removeItem(MODULOS_KEY)
}

export function getModulos() {
    return JSON.parse(localStorage.getItem(MODULOS_KEY));
}