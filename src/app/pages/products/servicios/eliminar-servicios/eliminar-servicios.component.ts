import { Component, Inject } from '@angular/core';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { IApiResponse } from '../../../../shared/models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'eliminar-servicios',
    templateUrl: './eliminar-servicios.component.html'
})
export class EliminarServiciosModalComponent {

    isLoading: boolean = false
    hasChange: boolean = false
    constructor(
        private _servicioService                  : ServicioService,
        private _matSnackBar                      : MatSnackBar,
        public  dialogRef                         : MatDialogRef<EliminarServiciosModalComponent>,
        @Inject(MAT_DIALOG_DATA) public servicioId: any
    ) { }

    async eliminarServicio() {
        try {
            this.isLoading = true
            const {
                Descripcion,
                Identificador,
            }: IApiResponse = await this._servicioService.deleteServicio(this.servicioId);
            if(Identificador === "sucess") {
                this.hasChange = true
                this.dialogRef.close()
            }
            __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error){
            console.log(error)
        } finally {
            this.isLoading = false
        }
        
    }

}