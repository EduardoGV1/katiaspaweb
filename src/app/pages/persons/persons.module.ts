import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { SharedModule } from '../../shared/shared.module';
import { PagesModule } from '../pages.module';
import { PersonsRoutingModule } from './persons.routing';
import { PersonalComponent } from './personal/personal.component';
import { EditarPersonalComponent } from './personal/editar/editar-personal.component';
import { AgregarServicioModalComponent } from './personal/editar/agregar-servicio/agregar-servicio-modal.component';
import { ClientesComponent } from './clientes/clientes.component';
import { DetalleClientesComponent } from './clientes/detalle-clientes/detalle-clientes.component';
import { AgregarPersonalComponent } from './personal/agregar/agregar-personal.component';
import { EditarPasswordComponent } from './personal/editar-password/editar-password-personal.component';
import { AgregarClienteComponent } from './clientes/agregar-clientes/agregar-clientes.component';
import { HorariosPersonalComponent } from './personal/horarios/horarios-personal.component';
import { Dias2LetrasPipe } from 'src/app/pages/pipes/dias2Letras.pipe';
import { EditarHorarioModalComponent } from './personal/horarios/editar-horarios/editar-horarios.component';

@NgModule({
  declarations: [
    PersonalComponent,
    EditarPersonalComponent,
    AgregarServicioModalComponent,
    ClientesComponent,
    DetalleClientesComponent,
    AgregarPersonalComponent,
    EditarPasswordComponent,
    AgregarClienteComponent,
    HorariosPersonalComponent,
    EditarHorarioModalComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PagesModule,
    PersonsRoutingModule,
    
  ],

})
export class PersonsModule { }
