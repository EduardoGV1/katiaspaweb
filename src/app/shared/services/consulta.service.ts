import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { COSULTORIO_URL } from 'src/app/utils/url_constants';


@Injectable({
    providedIn: 'root'
})
export class ConsultasService extends BaseService {
    constructor(
        private http: HttpClient
    ) {
        super();
    }
    listarConsultorioPaginado(
        obj: {
            Pagina: number;
            Cantidad: number;
        }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}ListarConsultorioPaginado`, obj, { headers: this.obtenerHeaders() }).toPromise();
    }
    registrarConsultorio(registrarCons: number): Promise<any> {
        const obj = {
            nroConsultorio: registrarCons
        }

        return this.http.post(`${COSULTORIO_URL}RegistrarConsultorio`, obj, { headers: this.obtenerHeaders() }).toPromise();
    }
    eliminarConsultorio(ConsultorioID: number): Promise<any> {
        const obj = {
            ConsultorioID: ConsultorioID
        }
        return this.http.post(`${COSULTORIO_URL}EliminarConsultorio`, obj, { headers: this.obtenerHeaders() }).toPromise();
    }
    listarServiciosDisponiblesPorConsultorioId(data: {
        consultorioID: number
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}ListarServiciosDisponiblesPorConsultorioID`, data, { headers: this.obtenerHeaders() }).toPromise();
    }
    listarServiciosRegistradoPorConsultorio(data: {
        ConsultorioID: number
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}ListarServiciosRegistradosPorConsultorio`, data, { headers: this.obtenerHeaders() }).toPromise();
    }
    asignarServicio(data: {
        ConsultorioID: number;
        ServicioID: number;
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}AsignarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
    }

    quitarServicio(data: {
        ConsultorioID: number;
        ServicioID: number;
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}QuitarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
    }

    desactivarServicio(data: {
        ConsultorioID: number;
        ServicioID: number;
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}DesactivarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
    }
    activarServicio(data: {
        ConsultorioID: number;
        ServicioID: number;
    }): Promise<any> {
        return this.http.post(`${COSULTORIO_URL}ActivarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
    }
    
}