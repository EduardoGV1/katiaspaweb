import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-pregunta-encuesta',
    templateUrl: './pregunta-encuesta.component.html'
})
export class PreguntaComponent implements OnInit {

    @Input () index        : number;
    @Input () idShow       : number;
    @Input () formPregunta : FormArray;
    @Output() show         : EventEmitter<number> = new EventEmitter();
    @Output() delete       : EventEmitter<number> = new EventEmitter();
    @Output() duplicate    : EventEmitter<number> = new EventEmitter();
    
    tipoPregunta: ITipoPregunta[]

    showComponent() {
        this.show.emit(this.index)
    }

    constructor() { 
        this.tipoPregunta = Tipos;
    }

    ngOnInit() { 
        this.formPregunta.get('tipoPregunta').valueChanges
            .subscribe(v => {
                this.formRespuesta.clear();
                const newFormControl = (v === "1") ? new FormControl(null) 
                                                    : new FormGroup({
                                                        respuestaID: new FormControl(0),
                                                        opcion     : new FormControl(null, [Validators.required]),
                                                    })
                this.formRespuesta.push(newFormControl)
            })
    }
    
    deletePregunta(index: number) {
        this.delete.emit(index)
    }

    duplicatePregunta(index: number) {
        this.duplicate.emit(index)
    }

    get formRespuesta() {
        return (this.formPregunta.get('respuesta') as FormArray);
    }
}

export interface ITipoPregunta {
    tipoID     : number;
    descripcion: string;
}

const Tipos: ITipoPregunta[] = [
    {
        tipoID: 1,
        descripcion: "Comentario",
    },
    {
        tipoID: 2,
        descripcion: "Opción Única",
    },
    {
        tipoID: 3,
        descripcion: "Opción Multiple",
    }
]