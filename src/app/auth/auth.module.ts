import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';

import { LayoutLoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    LayoutLoginComponent
  ],
  imports: [
    AuthRoutingModule,
    SharedModule,
    NgxSpinnerModule
  ],
  exports:[
    LayoutLoginComponent
  ]
})
export class AuthModule { }
