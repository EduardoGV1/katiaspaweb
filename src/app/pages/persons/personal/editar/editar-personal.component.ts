import { Component, OnInit } from '@angular/core';
import { IApiResponse, IRoles } from '../../../../shared/models/response';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { PersonsService } from 'src/app/shared/services/persons.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AgregarServicioModalComponent } from '../../personal/editar/agregar-servicio/agregar-servicio-modal.component';
import { ObvsService } from 'src/app/shared/services/obvs.service';
import * as moment from 'moment';
import { RolesService } from 'src/app/shared/services/roles.Service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
const ToDay = moment(new Date()).add(-18, 'years').toDate();

@Component({
  selector: 'app-editar-personal',
  templateUrl: './editar-personal.component.html',
  styleUrls: ["./editar-personal.component.scss"],
})
export class EditarPersonalComponent implements OnInit {

  formularioGroup: FormGroup
  idPersonal: number
  trabajadoresArr = []
  rolesArr = []
  trabajador: any;
  nombres: string = '';
  apellidos: string = '';
  dni: string = '';
  fechaNacimiento = null;
  fotoTrabajador: any = ""

  existFoto: boolean = false
  fotoAdd =null;

  disabled: boolean = true;

  disabledEndDate = (startValue: Date): boolean => {
    if (!startValue || !ToDay) {
      return false;
    }
    return startValue.getTime() > ToDay.getTime();
  };

  constructor(
    private _personsService: PersonsService,
    private _matSnackBar: MatSnackBar,
    private _activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    public _obvsService: ObvsService,
    private _rolesService: RolesService,
    private firestore: AngularFirestore,
    private http: HttpClient,
    public datePipe: DatePipe,
  ) {
    _activeRoute.paramMap.subscribe((value: ParamMap) => this.idPersonal = Number(value.get('id')));
    this.formularioGroup = new FormGroup({
      nombre: new FormControl(null, [Validators.required]),
      apellido: new FormControl(null, [Validators.required]),
      dni: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(9)]),
      telefono: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(10)]),
      correo: new FormControl(null, [Validators.required, Validators.email]),
      genero: new FormControl("", [Validators.required]),
      rolID: new FormControl("", [Validators.required]),
      direccion: new FormControl(null, [Validators.required]),
      fecha: new FormControl(null, [Validators.required])
    })
  }

  async ngOnInit() {
    this.formularioGroup.disable();
    await this.obtenerInformacionPersonal();
    this.obtenerRoles();
  }

  async obtenerInformacionPersonal() {
    try {
      this._obvsService.toogleSpinner()
      const { Identificador, Mensaje, Descripcion, Resultado }: IApiResponse = await this._personsService.obtenerTrabajadorPorID({ usuarioID: this.idPersonal });
      if (Identificador == "success") {
        this.trabajador = Resultado;
        this.trabajador['nombres'] = Resultado.Nombres
        this.formularioGroup.patchValue({
          nombre: Resultado.Nombres,
          apellido: Resultado?.Apellidos,
          dni: Resultado?.DNI,
          telefono: Resultado?.Telefono,
          correo: Resultado?.Correo,
          genero: Resultado?.Genero,
          rolID: Resultado?.Rol?.RolID,
          direccion: Resultado?.Direccion,
          fecha: new Date(Resultado?.FechaNacimiento)
        })
        this.fotoTrabajador = Resultado.Foto
        if (Resultado.Foto) {
          this.existFoto = true
        }

      } else {
        return __messageSnackBar(this._matSnackBar, Descripcion)
      }
    } catch (error) {
      return __messageSnackBar(this._matSnackBar, error)
    } finally {
      this._obvsService.toogleSpinner()
    }
  }

  // async getFotosFB(fotosFB: string) {
  //   this.http.get(fotosFB)
  //     .subscribe(data => {
  //     }, error => {
  //       console.error('Error en la solicitud GET:', error);
  //     });
  // }

  async obtenerRoles() {
    try {
      const {
        Mensaje,
        Identificador,
        Descripcion,
        Roles
      }: IRoles = await this._rolesService.listarRoles({
        Start: 1,
        Length: 20
      })
      this.rolesArr = Roles
      console.log(this.rolesArr)
    } catch (error) {
      console.log(error)
    }
  }

  async editarTrabajdorByID() {
    console.log(this.trabajador)
    console.log("log update", this.formularioGroup)
    this.formularioGroup.disable();
    this._obvsService.toogleSpinner()
    try {
      
      let foto = (this.fotoTrabajador=="" || this.fotoTrabajador==null || this.fotoTrabajador ==undefined)?null:this.fotoTrabajador;
      let fechaNacimiento = this.datePipe.transform(this.formularioGroup.get('fecha').value, 'yyyy-MM-dd')
      const {Identificador,Mensaje,Descripcion }: IApiResponse = await this._personsService.actualizarPerfilTrabajador({
        workerID: this.idPersonal,
        photo: foto,
        name: this.formularioGroup.get('nombre').value,
        surname: this.formularioGroup.get('apellido').value,
        email: this.formularioGroup.get('correo').value,
        phone: this.formularioGroup.get('telefono').value,
        fechaNac: fechaNacimiento,
        direccion: this.formularioGroup.get('direccion').value,
        genero: this.formularioGroup.get('genero').value,
        existeFoto: this.existFoto,
        rolID: this.formularioGroup.get('rolID').value
      })
      if (Identificador=="success") {
        this.obtenerInformacionPersonal();
        this.disabled=true
      }
    } catch (error) {
      console.log(error)
    } finally {
      console.log(123)
      this._obvsService.toogleSpinner()
    }
  }

  habilitarEdicion(value: boolean) {
    (value) ? this.formularioGroup.disable() : this.formularioGroup.enable()
    this.disabled = value;
  }

  addFotos() {
    const input: HTMLInputElement = document.querySelector('#fileFotos');
    this.fileTo64Bits(input.files);
    console.log(input.files);
    // this.fotoAdd=input.files
  }

  fileTo64Bits(imagen: FileList) {
    const foto = imagen[0]
    if (foto.type.startsWith("image")) {
      const reader = new FileReader();
      reader.readAsDataURL(foto);
      reader.onloadend = () => {
        const newFile: any = reader.result;
        this.fotoAdd = newFile
        this.fotoTrabajador=newFile.split(',')[1];
        //.split(',')[1];


        console.log(newFile);
      }
    } else {
      console.log("Error")
    }
  }

  agregarServicio() {
    this.dialog.open(AgregarServicioModalComponent, {
      disableClose: true,
      data: this.idPersonal
    })
  }
}
