import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EncuestaService } from '../../../shared/services/encuesta.service';
import { IApiResponse } from '../../../shared/models/response';

@Component({
    selector   : 'app-encuesta',
    templateUrl: './encuesta.component.html'
})
export class EncuestaComponent implements OnInit {
    
    encuestaArr = []

    constructor(
        private _router         : Router,
        private _activatedRoute : ActivatedRoute,
        private _encuestaService: EncuestaService,
    ) { }
    
    async ngOnInit() {
        const {
            Resultado
        }: IApiResponse = await this._encuestaService.getEncuestaPaginado({pages: 1, rows: 10});
        this.encuestaArr = Resultado;
    }

    agregarEncuesta(encuestaId: number = 0) {
        let ruta = ['nueva'];
        if(encuestaId > 0) ruta = [encuestaId.toString(), 'editar']
        this._router.navigate(ruta, {relativeTo: this._activatedRoute})
    }

    visualizarEncuesta(encuestaId: number = 0) {
        this._router.navigate([encuestaId.toString(), 'ver'], {relativeTo: this._activatedRoute})
    }

    eliminarEncuesta(encuestaId: number = 0) {

    }
    
}