import { Component, OnInit } from '@angular/core';
import { CalendarOptions, EventAddArg } from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import esLocale from '@fullcalendar/core/locales/es'
import { CitaService } from '../../../shared/services/cita.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { DialogReservarCitaComponent } from './dialog-reservar-cita/dialog-reservar-cita.component';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

export interface CitaCalendario  {
  id: string;
  title: string;
  date: string;
  allDay: boolean;
  end: string;
}

export interface Citas {
  ConsultorioID: number;
  NroConsultorio: number;
  Citas: Cita[];
}

export interface Cita {
  ApellidoCliente: string,
  ClienteID: number,
  Duracion: number,
  FechaHora: Date,
  FotoCliente: string,
  NombreCliente: string,
  NombreServicio: string,
  ReservaSesionID: number,
  CitaSesionID: number,
  CitaID: number,
  EstadoString: string;
  Estado: string;
  ServicioID: number;
  UsuarioID: number;
}

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.scss']
})
export class CitaComponent implements OnInit {

  /////INICIO

  listaHorarios = [
    "8:00-8:15","8:15-8:30","8:30-8:45","8:45-9:00",
    "9:00-9:15","9:15-9:30","9:30-9:45","9:45-10:00",
    "10:00-10:15","10:15-10:30","10:30-10:45","10:45-11:00",
    "11:00-11:15","11:15-11:30","11:30-11:45","11:45-12:00",
    "12:00-12:15","12:15-12:30","12:30-12:45","12:45-13:00",
    "13:00-13:15","13:15-13:30","13:30-13:45","13:45-14:00",
    "14:00-14:15","14:15-14:30","14:30-14:45","14:45-15:00",
    "15:00-15:15","15:15-15:30","15:30-15:45","15:45-16:00",
    ]
  listOfData = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      tel: '0571-22098909',
      phone: 18889898989,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      tel: '0571-22098333',
      phone: 18889898888,
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      tel: '0575-22098909',
      phone: 18900010002,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '4',
      name: 'Jim Red',
      age: 18,
      tel: '0575-22098909',
      phone: 18900010002,
      address: 'London No. 2 Lake Park'
    },
    {
      key: '5',
      name: 'Jake White',
      age: 18,
      tel: '0575-22098909',
      phone: 18900010002,
      address: 'Dublin No. 2 Lake Park'
    }
  ];
  /////FIN

  fechaHoy: any;
  citas: Citas[];
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',//dayGridWeek
    dateClick: this.handleDateClick.bind(this),
    eventClick: this.eventClick.bind(this),
    locale: esLocale,
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,dayGridWeek,dayGridDay,timeGridWeek'
    },
    dayMaxEvents: true,
    dayMaxEventRows: 5,
    // events: [
    //   {  title: 'Evento 1', date: '2021-06-30 19:30:00', allDay: false,end: '2021-06-30 20:30:00', id: '1'   },
    //   { title: 'Evento 2', date: '2021-06-29 19:30:00',allDay: false,end: '2021-06-29 20:30:00' }
    // ],
    eventTimeFormat: {
      hour: '2-digit',
      minute: '2-digit',
      meridiem: false
    },
    showNonCurrentDates: false,
    eventResize: function(info){
      return 'vertical'
    }
  }

  constructor(private citaService: CitaService, public dialog: MatDialog, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    await this.mostrarEventos();
    
  }

  async mostrarEventos() {
    const fechaHoy = moment().format('yyyy-MM-DD');
    const fechaFin = moment(fechaHoy).add(2, 'month').format('yyyy-MM-DD');
    this.addCitasCalendar([]);
    this.spinner.show();
    this.citas = await this.obtenerCitasWebPorFecha(fechaHoy,fechaFin);
    this.spinner.hide();
    console.log('CITAS',this.citas);
    let calendario: CitaCalendario[] = [];
    for (const item of this.citas) {
      for (const cita of item.Citas) {
        let fechaHoraInicio = moment(cita.FechaHora).format('yyyy-MM-DD HH:mm:ss')
        let fechaHoraFin = moment(cita.FechaHora).add(cita.Duracion, 'minutes').format('yyyy-MM-DD HH:mm:ss');
        calendario.push({ title: cita.NombreServicio+" | "+cita.NombreCliente+" | "+cita.ApellidoCliente + " Nro Consultorio: "+item.NroConsultorio+" | Estado Sesion: "+cita.EstadoString, date: fechaHoraInicio,allDay: false, end: fechaHoraFin, id: cita.ReservaSesionID.toString()  })  
      }
      
    }
    this.addCitasCalendar(calendario);
    console.log('CITAS',this.citas);
  }

  async obtenerCitasSemana(): Promise<Citas[]> {
    // const fechaHoy = moment().format('yyyy-MM-DD');
    // const fechaFin = moment(fechaHoy).add(2, 'month').format('yyyy-MM-DD');
    // // let fechaHoy = '2021-06-30';
    // let citas: Citas[] = [];
    // for (let index = 0; index < 7; index++) {
    //   let nuevaFecha = moment(fechaHoy).add(index,'day').format('yyyy-MM-DD');
    //   let cita = await this.obtenerCitasWebPorFecha(nuevaFecha);
    //   citas.push(...cita);
      
    // }
    // // this.citas = await this.obtenerCitasWebPorFecha(fechaPrueba);
    // return citas;
    return null;
  }

  async handleDateClick(arg) {
    console.log('ARG', arg);
    const fechaHoy = moment(moment().format('yyyy-MM-DD'),'yyyy-MM-DD');
    const fechaSeleccionada = moment(arg.dateStr);
    const fechaDosMesesDespues = moment(moment().add(60,'day').format('yyyy-MM-DD'),'yyyy-MM-DD');
    if(fechaSeleccionada > fechaDosMesesDespues) {
      await Swal.fire({
        title: 'No puede crear una cita a una fecha máximo 60 días',
        icon: 'warning',
        timer: 2500
      })
    }
    else if(fechaSeleccionada < fechaHoy){
      await Swal.fire({
        title: 'No puede crear una cita a una fecha anterior a la de hoy',
        icon: 'warning',
        timer: 2500
      })
      return;
    } else {
      this.reservarCita(arg.dateStr);
      return;
    }
    
    
  }

  eventClick(arg) {
    console.log('ARG',arg.event.id);
    // const cita = this.citas.find(item => item.Citas.filter(x => x.ReservaSesionID === Number.parseInt(arg.event.id)  )[0].ReservaSesionID === arg.event.id).Citas;  //
    // let vea = this.citas[0].Citas.find(item => item.ReservaSesionID === Number.parseInt(arg.event.id));
    let temp  = undefined;
    let citaValue: Cita;
    for (const iterator of this.citas) {
      for (const cita of iterator.Citas) {
        if(cita.ReservaSesionID === Number.parseInt(arg.event.id)) {
          temp = iterator.ConsultorioID;
          citaValue = cita;
          break;
        }
        if(temp != undefined) {
          break;
        }
      }
    }
    let fechaHoy = moment(citaValue.FechaHora).format('yyyy-MM-DD');
    const dialogRef = this.dialog.open(DialogReservarCitaComponent ,{
      width: '50%',
      height: '50%',
      data: {
        fechaHoy: fechaHoy,
        tipo: 2, // Tipo 1: Creación , Tipo 2: Editar,
        cita: citaValue,
        consultorioId: temp
      }
    });

    dialogRef.afterClosed().subscribe(async  result => {
      if(result == 1) {
        await this.mostrarEventos();

      }
    })

  }

  async obtenerCitasWebPorFecha(fechaHoy: string, fechaFin: string): Promise<Citas[]> {
    try {
      const resp = await this.citaService.obtenerCitasWebPorFecha({ fechaInicio: "2023-09-01", fechaFin: fechaFin });
      // const resp = await this.citaService.obtenerCitasWebPorFecha({ fechaInicio: fechaHoy, fechaFin: fechaFin });
      console.log('RESP',resp);
      let citas: Citas[] = resp.Sesiones;
      
      return citas;
    } catch (error) {
      console.log('ERROR',error); 
    }
  }

  addCitasCalendar(citas: CitaCalendario[]): void {
    let event: EventAddArg;
    this.calendarOptions.events = citas;
    // this.calendarOptions.events = this.citas.map()
  }

  reservarCita(fechaHoy: string) {
    console.log('RESERVAR CITA');
    const dialogRef = this.dialog.open(DialogReservarCitaComponent ,{
      width: '50%',
      height: '50%',
      data: {
        fechaHoy: fechaHoy,
        tipo: 1 // Tipo 1: Creación , Tipo 2: Editar
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if(result == 1) {
        await this.mostrarEventos();

      }
    })
  }

}
