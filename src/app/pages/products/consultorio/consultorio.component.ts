import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IApiResponse } from 'src/app/shared/models/response';
import { ConsultasService } from 'src/app/shared/services/consulta.service';
import { ServicioService } from 'src/app/shared/services/servicio.service';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { AgregarConsultorioModalComponent } from './agregar-consulorio/agregar-consultorio.component';
import { ConsultarDetalleModalComponent } from './detalle-consultorio/detalle-consultorio.component';
import { ObvsService } from 'src/app/shared/services/obvs.service';


@Component({
  selector: 'app-consultorio',
  templateUrl: './consultorio.component.html'
})
export class ConsultorioComponent implements OnInit {

  consultoriosArr: any = []
  formulariogroup: FormGroup
  actualizado:boolean=false

  constructor(
    
    private _cconsultaService: ConsultasService,
    public dialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    public _obvsService: ObvsService,
  ) {
  }

  async listarConsultas() {
    try {
      this._obvsService.toogleSpinner()
      const {
        Resultado
      }: IApiResponse = await this._cconsultaService.listarConsultorioPaginado({
        Pagina: 1,
        Cantidad: 10
      });
      this.consultoriosArr = Resultado
    } catch (error) {
      console.log(error)
    } finally{
      this._obvsService.toogleSpinner()
    }
  }

  async ngOnInit() {
    await this.listarConsultas();

  }
  async agregarConcultorio() {
    const dialogRef = this.dialog.open(AgregarConsultorioModalComponent)
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.actualizado) {
        this.ngOnInit()
      }
    })
  }
  async detalleconsultorio(consultorioID: number){
    const dialogRef = this.dialog.open(ConsultarDetalleModalComponent, {
      data: consultorioID
    })
  }
  
  async EliminarConsultorio(ConsultorioID: number) {
    try {
      
      const {
        Descripcion,
        Identificador,
        Mensaje,
        Resultado
      }: IApiResponse = await this._cconsultaService.eliminarConsultorio(
        ConsultorioID)
      if (Identificador === "success") {
        console.log("Eliminado")
        this.ngOnInit()
      }
      return __messageSnackBar(this._matSnackBar, Descripcion)
    } catch (error) {
      console.log("Error")
    }
    
  }
}


