import { Directive, HostListener, EventEmitter, Output, Input } from '@angular/core';

@Directive({
  selector: '[appDropZone]'
})
export class DropZoneDirective {
  @Output() emitFotos   : EventEmitter<File[]> = new EventEmitter()
  @Output() emitDropZone: EventEmitter<boolean> = new EventEmitter()
  @Input () fotos = []
  
  constructor() { }

  @HostListener('dragover', ['$event'])
  public onDragEnter(event: any) {
    this.emitDropZone.emit(true);
    this._preventEvent(event);
  }

  @HostListener("dragleave", ["$event"])
  public onLeaveEvent(event) {
    this.emitDropZone.emit(false)
  }

  @HostListener("drop", ["$event"])
  public onDrop(event) {
    const tranferencia = this._getFotos(event)
    if (!tranferencia) return
    this.extractFiles(tranferencia.files)
    this._preventEvent(event)
    this.emitDropZone.emit(false)
  }

  private _getFotos(event: any) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer
  }

  private _archivoPuedeSerCargado(file: File) {
    if (this.fotos.length == 5) return false
    if (!this.validateFoto(file.name) && this._esImagen(file.type)) return true
    return false
  }

  private _preventEvent(event) {
    event.preventDefault()
    event.stopPropagation()
  }

  private validateFoto(name) {
    for (const foto of this.fotos) {
      if (name == foto.nombreArchivo) return true
    }
    return false
  }

  private _esImagen(type: string) {
    return (type === '' || type === undefined) ? false : type.startsWith('image');
  }

  private extractFiles(files: FileList) {
    const newFiles = []
    for (const propiedad in Object.getOwnPropertyNames(files)) {
      const temp = files[propiedad];
      if (this._archivoPuedeSerCargado(temp)) {
        newFiles.push(temp)
      }
    }
    this.emitFotos.emit(newFiles)
    this.fotos = []
  }
}

export class FileItem {

    public archivo      : File;
    public nombreArchivo: string;
    public url          : string;
    public estaSubiendo : boolean;
    public progreso     : number;
    public dataFile     : string |  ArrayBuffer

    constructor( archivo: File, dataFile ) {

        this.archivo       = archivo;
        this.nombreArchivo = archivo.name;
        this.estaSubiendo  = false;
        this.progreso      = 0;
        this.dataFile      = dataFile

    }

}