import { Pipe, PipeTransform } from "@angular/core";


@Pipe({
    name:'dias2Letras'
})

export class Dias2LetrasPipe implements PipeTransform{
    transform(value: string):string {
        console.log("value",value)
        if(value=="LU") return value="LUNES"
        if(value=="MA") return value="MARTES"
        if(value=="MI") return value="MIERCOLES"
        if(value=="JU") return value="JUEVES"
        if(value=="VI") return value="VIERNES"
        if(value=="SA") return value="SABADO"
    }
}