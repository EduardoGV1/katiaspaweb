import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { IApiResponse, ICategoriasModel } from 'src/app/shared/models/response';
import { ServicioService } from 'src/app/shared/services/servicio.service';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { AgregarServiciosModalComponent } from '../servicios/agregar-servicios/agregar-servicios.component';
import { AgregarCategoriaServiciosModalComponent } from './agregar-categoria-servicios/agregar-categoria-servicios.component';
import { EditarCategoriaServiciosModalComponent } from './editar-categorias-servicios/editar-categoria-servicios.component';
import { ObvsService } from 'src/app/shared/services/obvs.service';

@Component({
    selector: 'app-categoria-servicios',
    templateUrl: 'categoria-servicios.component.html'
})
export class CategoriaServiciosComponent implements OnInit {
    displayedColumns: string[] = ['CategoriaServicioID', 'Nombre Categoría', 'Total de servicios activos', 'Acción'];
    paginatorPageSize = 10;
    categoriasArr: any = []
    start: number = 0
    size: number = 10;
    totalRecords=0;

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar: MatSnackBar,
        public dialog: MatDialog,
        public _obvsService: ObvsService,
    ) { }

    async ngOnInit() {
        this.listarCategorias(this.start, this.size);
    }
    async listarCategorias(start: number, size: number) {
        try {
            this._obvsService.toogleSpinner()
            const data = await this._servicioService.listarCategoriaServicios({ pagina: start, cantidad: size });
            this.categoriasArr = data.Categorias
            this.totalRecords = data.Total
            console.log(data)
        } catch (err) {
            __messageSnackBar(this._matSnackBar, err.Descripcion)
        } finally {
            this._obvsService.toogleSpinner()
        }
    }

    agregarCategoriaServicios() {
        const dialogRef = this.dialog.open(AgregarCategoriaServiciosModalComponent, { disableClose: true })
        dialogRef.afterClosed().subscribe(() => {
            if (dialogRef.componentInstance.changeData) this.ngOnInit()
        })
    }
    async eliminarCategoriaServicio(categoriaServicioID: number) {
        try {
            const {
                Descripcion,
                Identificador,
                Mensaje,
                Resultado
            }: IApiResponse = await this._servicioService.deleteCategoriaServicio(
                categoriaServicioID)
            if (Identificador === "success") {
                this.ngOnInit()
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log("Error")
        }
    }

    editarCategoriaServicio(categoriaServicioID: number, Descripcion: string) {
        const dialogRef = this.dialog.open(EditarCategoriaServiciosModalComponent, {
            data: {
                categoriaSerId: categoriaServicioID,
                descripcionCatServ: Descripcion
            },
            disableClose: true
        })
        dialogRef.afterClosed().subscribe(() => {
            if (dialogRef.componentInstance.changeData) this.ngOnInit()
        })

    }
    async paginacion(event: any) {
        console.log("event", event)
        await this.listarCategorias(event.pageIndex * this.paginatorPageSize, this.paginatorPageSize)


    }
}