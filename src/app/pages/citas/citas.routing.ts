import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PagesComponent } from '../pages.component';
import { CitaComponent } from './cita/cita.component';


const routes: Routes = [
    {
        path    : '',
        component: PagesComponent,
        children: [
            {
                path: 'Citas',
                data: { breadcrumb: "Citas" },
                children: [
                    {
                        path:"Schedule",
                        component:CitaComponent
                    }
                ]
            }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CitasRoutingModule { }
