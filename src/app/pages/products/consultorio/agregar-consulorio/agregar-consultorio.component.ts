import { Inject, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IApiResponse } from 'src/app/shared/models/response';
import { ConsultasService } from 'src/app/shared/services/consulta.service';
import { __messageSnackBar } from 'src/app/utils/helpers';

@Component({
    selector: 'agregar-consultorio',
    templateUrl: './agregar-consultorio.component.html'
})
export class AgregarConsultorioModalComponent implements OnInit{
    formularioGroup: FormGroup
    actualizado:boolean=false

    constructor(
        public dialog: MatDialog,
        private _matSnackBar: MatSnackBar,
        public dialogRef: MatDialogRef<AgregarConsultorioModalComponent>,
        private _cconsultaService: ConsultasService,
        
        @Inject(MAT_DIALOG_DATA) public servicioId: any
    ) { this.formularioGroup= new FormGroup({
        nConsultorio: new FormControl(null,[Validators.required])
    }) }

    async registrarconsultorio() {
        try {
            const nroConsultorio = Number(this.formularioGroup.get('nConsultorio').value)
            this.formularioGroup.disable();
            const {
                Descripcion,
                Identificador,
                Mensaje
                
            }: IApiResponse = await this._cconsultaService.registrarConsultorio(nroConsultorio)
            if(Identificador==="success"){
                this.dialogRef.close();
                this.actualizado=true
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log(error);
        }
        finally { this.formularioGroup.enable() }
    }
    async ngOnInit(){
        
    }
}