import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ObvsService } from '../services/obvs.service';
import { getUserData } from 'src/app/utils/storage';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  breadCrum: any
  user:any
  constructor(
    private _authService: AuthService,
    private _obvsService: ObvsService
  ) { 
    _obvsService.$breadcrumbsObvs.subscribe(value => this.breadCrum = value)
  }

  ngOnInit(): void {
    this.user = getUserData();
  }

  logOut() {
    this._authService.logOut();
  }

}
