import { Component, OnInit,Inject } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { IApiResponse } from '../../../../shared/models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { inject } from '@angular/core/testing';

@Component({
    selector: 'editar-categoria-servicios',
    templateUrl: './editar-categoria-servicios.component.html'
})
export class EditarCategoriaServiciosModalComponent implements OnInit{
    formularioGroup: FormGroup
    changeData     : boolean = false

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar :    MatSnackBar,
        public dialogRef : MatDialogRef<EditarCategoriaServiciosModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data:any
    ){
        this.formularioGroup=new FormGroup({
            descripcion: new FormControl(data.descripcionCatServ,[Validators.required])
        })
    }

    async editarCategoriaServicios(){
        try {
            console.log(this.data.categoriaSerId)
            this.formularioGroup.disable();
            if(this.data.categoriaSerId){
                const {
                    Identificador,
                    Resultado,
                    Descripcion
                }: IApiResponse=await this._servicioService.updateCategoriaServicio(this.data.categoriaSerId ,this.formularioGroup.get('descripcion').value);
                if(Identificador === "success") {
                    this.dialogRef.close()
                    this.changeData=true
                }
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
        } catch (error) {
            console.log("Error")
        }finally{
            this.formularioGroup.enable();
        }


    }




    async ngOnInit(){
        

    }
}