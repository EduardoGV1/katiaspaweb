import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { VENTAS_URL } from 'src/app/utils/url_constants';


@Injectable({
    providedIn: 'root'
  })
  export class SalesService extends BaseService {
  
    constructor(
      private http: HttpClient
    ) {
      super();
    }

    obtenerVentasPorRango(
        obj: {
            fechaInicio: string;
            fechaFin: string;
            pagina: number;
            cantidad: number;
        }): Promise<any> {
        return this.http.post(`${VENTAS_URL}ObtenerVentasPorRango`, obj, { headers: this.obtenerHeaders() }).toPromise();
      }

      listarFechasPagosxMes(
        obj: {
          year: number;
          month: number
           
        }): Promise<any> {
        return this.http.post(`${VENTAS_URL}ListarFechasPagosxMes`, obj, { headers: this.obtenerHeaders() }).toPromise();
      }

      obtenerServiciosVentaxRangoFecha(
        obj: {
          fechaInicio: string;
          fechaFin: string
           
        }): Promise<any> {
        return this.http.post(`${VENTAS_URL}ObtenerServiciosVentaxRangoFecha`, obj, { headers: this.obtenerHeaders() }).toPromise();
      }

      listarFechasPagosxYear(
        obj: {
          year: number;
           
        }): Promise<any> {
        return this.http.post(`${VENTAS_URL}ListarFechasPagosxYear`, obj, { headers: this.obtenerHeaders() }).toPromise();
      }

      listarReportePaginado(
        obj:{
          pagina:number,
          cantidad:number
        }
      ):Promise<any>{
        return this.http.post(`${VENTAS_URL}ListarReportePaginado`,obj,{headers:this.obtenerHeaders()}).toPromise();
      }

}