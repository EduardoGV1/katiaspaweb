import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogData } from 'src/app/pages/persons/personal/personal.component';
import { IApiResponse } from 'src/app/shared/models/response';
import { RolesService } from 'src/app/shared/services/roles.Service';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { DialogData1 } from '../rol.component';

@Component({
  selector: 'app-editar-rol',
  templateUrl: './editar-rol.component.html',
})
export class EditarRolComponent implements OnInit {
  changeData = false;
  formularioGroup: FormGroup;

  constructor(
    private _rolesService: RolesService,
    public dialogRef: MatDialogRef<EditarRolComponent>,
    private _matSnackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data:DialogData1
  ) {
    this.formularioGroup = new FormGroup({
      descripcion: new FormControl(data.descripcion, [Validators.required]),
    });
  }

  ngOnInit() {}
  async editarRol() {
    try {
      const { Identificador, Mensaje, Descripcion }: IApiResponse =
        await this._rolesService.editarRol({
            rolID:this.data.rolID,
            descripcion: this.formularioGroup.get('descripcion').value,
        });
      if (Identificador === 'success') {
        this.dialogRef.close();
        this.changeData = true;
      }
      return __messageSnackBar(this._matSnackBar, Descripcion);
    } catch (error) {
      console.log(error);
    } finally {
      this.formularioGroup.enable();
    }
  }
}
