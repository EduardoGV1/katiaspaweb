import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DetalleCitaComponent } from "./detalle-cita/detalle-cita.component";
import { CitaService } from "src/app/shared/services/cita.service";
import { IApiResponse } from "src/app/shared/models/response";
import { MatSnackBar } from "@angular/material/snack-bar";
import { __messageSnackBar } from "src/app/utils/helpers";
import { PagoComponent } from "./pago/pago.component";
import { DevolucionComponent } from "./devolucion/devolucion.component";
import { ServicioService } from "src/app/shared/services/servicio.service";
import { ObvsService } from "src/app/shared/services/obvs.service";
import { PersonsService } from "src/app/shared/services/persons.service";

@Component({
    selector: 'app-citasgestion',
    templateUrl: './citas.component.html'
})

export class CitasGestionComponent implements OnInit {
    listaCitas: any[] = [];
    serviciosArr = [];
    paginatorPageSize = 10;
    sizeTotal: number = 0;
    displayedColumnsCitas: string[] = ['NroCita', 'NombreServicio', 'Cliente', 'SesionActualTotal', 'MontoServicio', 'MontoDevuelto', 'EstadoPago', 'EstadoCita', 'FechaReserva', 'EstadoCitaSesion', 'FechaUltimaReserva', 'NombreEspecialista', 'NroConsultorio', 'Ver', 'Devolucion'];
    //Campos Filtros
    comboEstado: string = "";
    rangoFechas: Array<string> = [];
    nombresCliente: string = "";
    comboServicios: string = "";
    comboPago: string = "";
    comboEspecialista: string = "";
    listaEstadosCitaSesion: any[] = [];
    listaEstadosCitas: any[] = [];
    listaEspecialitas= [];
    numCita: any = "";
    numConsultorio: any;

    constructor(
        private _servicioService: ServicioService,
        public dialog: MatDialog,
        private _citaService: CitaService,
        private _matSnackBar: MatSnackBar,
        private _personsService:PersonsService,
        public _obvsService: ObvsService,
    ) { }
    listaEstados = [
        {
            label: "En Proceso",
            value: "1"
        },
        {
            label: "Stand By",
            value: "2"
        },
        {
            label: "Cancelado",
            value: "3"
        },
        {
            label: "Terminado",
            value: "4"
        },
        {
            label: "Prueba",
            value: "5"
        }
    ];
    listaEstadoPago = [
        {
            label: "Pagado",
            value: "1"
        },
        {
            label: "Sin Pago",
            value: "0"
        }
    ]
    ngOnInit(): void {
        console.log("error")
        this.listarCitas(0, this.paginatorPageSize);
        this.listarServicios();
        this.obtenerEstadosCitaSesion();
        this.obtenerEstadosCitas();
        this.listarEspecialistas();
    }

    async listarEspecialistas(){
        try {
            const {Identificador,Mensaje,Descripcion,Resultado}:IApiResponse= await this._personsService.obtenerEspecialistas();
            this.listaEspecialitas=Resultado
        } catch (error) {
            
        }
    }

    verDetalleCita(cita: any) {
        console.log("dev1", cita)
        debugger
        let estadoPago = (cita.EstadoPago==0)?false:true;
        const dialogRef = this.dialog.open(DetalleCitaComponent, {
            width: '100%',
            disableClose: true,
            data: { citaID: cita.CitaID, servicioID: cita.ServicioID, clienteNombre: cita.ClienteNombre, clienteID: cita.ClienteID, duracion: cita.DuracionServicio, sesionTotal: cita.SesionTotal,estadoPago:estadoPago }
        })
        dialogRef.afterClosed().subscribe(() => {
            //validar la data de retorno y call ngoinit() si es exitoso
            this.ngOnInit();
        })
    }
    async listarCitas(start: number, length: number) {
        try {
            this._obvsService.toogleSpinner()
            let estado = (this.comboEstado == null || this.comboEstado == undefined || this.comboEstado == "") ? null : this.comboEstado;
            let fechaInicio = (this.rangoFechas[0] == null || this.rangoFechas[0] == undefined || this.rangoFechas[0] == "") ? null : this.rangoFechas[0];
            let fechaFin = (this.rangoFechas[1] == null || this.rangoFechas[1] == undefined || this.rangoFechas[1] == "") ? null : this.rangoFechas[1]
            let nombresCliente = (this.nombresCliente == null || this.nombresCliente == undefined || this.nombresCliente == "") ? null : this.nombresCliente;
            let servicioID = (this.comboServicios == null || this.comboServicios == undefined || this.comboServicios == "") ? null : this.comboServicios;
            let estadoPago = (this.comboPago == null || this.comboPago == undefined || this.comboPago == "") ? null : this.comboPago;
            let numCita = (this.numCita == null || this.numCita == undefined || this.numCita == "") ? null : this.numCita;
            let numConsultorio = (this.numConsultorio == null || this.numConsultorio == undefined || this.numConsultorio == "") ? null : this.numConsultorio;
            let usuarioID = (this.comboEspecialista == null || this.comboEspecialista == undefined || this.comboEspecialista == "") ? null : this.comboEspecialista;

            const { Descripcion, Identificador, Resultado, Citas, Paginado }: IApiResponse = await this._citaService.obtenerCitasWebPaginado(
                {
                    pagina: start,
                    registros: length,
                    estado: estado,
                    fechaInicio: fechaInicio,
                    fechaFin: fechaFin,
                    clienteNombres: nombresCliente,
                    servicioID: servicioID,
                    estadoPago: estadoPago,
                    numCita:numCita,
                    numConsultorio:numConsultorio,
                    usuarioID:usuarioID
                }
            );
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.listaCitas = [];
            this.listaCitas = Citas
            this.sizeTotal = Paginado.totalRegistros
            console.log(this.listaCitas)
        } catch (error) {
            console.log(error)
        } finally{
            this._obvsService.toogleSpinner()
        }
    }
    buscarCitas() {
        console.log(this.comboEstado)
        console.log(this.rangoFechas)
        console.log(this.nombresCliente)
        console.log(this.comboServicios)
        console.log(this.comboPago)
        console.log(this.numConsultorio)
        console.log(this.numCita)
        console.log(this.comboEspecialista)
        this.listarCitas(0, this.paginatorPageSize);

    }
    limpiarFiltros() {
        this.comboEstado = null;
        this.rangoFechas = [];
        this.nombresCliente = "";
        this.comboServicios = null;
        this.comboPago = null;
        this.numCita = null;
        this.numConsultorio = null;
        this.comboEspecialista = null;
    }
    async listarServicios(filtros?: any) {
        debugger
        var filtro = {
            isActivo: 1,
            genero: null,
            pagina: 1,
            cantidad: 10,
            categoriaServicioID: 0
        }
        if (filtros) {
            filtro.isActivo = (filtros.isActivo && filtros.isActivo != "") ? Number(filtros.isActivo) : 2;
            filtro.genero = (filtros.genero && filtros.genero != "") ? filtros.genero : null;
            filtro.categoriaServicioID = (filtros.categoriaServicioID && filtros.categoriaServicioID != "") ? Number(filtros.categoriaServicioID) : 0;
        }
        try {
            const {
                Descripcion,
                Identificador,
                Resultado
            }: IApiResponse = await this._servicioService.listarServiciosPaginado(filtro)

            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.serviciosArr = Resultado
        } catch (error) {
            console.log(error)
        }
    }
    async obtenerEstadosCitaSesion() {
        try {
            const { Identificador, Mensaje, Descripcion, Estados }: IApiResponse = await this._citaService.obtenerEstadosCitaSesion()
            if (Identificador == "success") {
                this.listaEstadosCitaSesion = Estados
                console.log(this.listaEstadosCitaSesion)
            }
        } catch (error) {

        }
    }

    async obtenerEstadosCitas() {
        try {
            const { Identificador, Mensaje, Descripcion, Estados }: IApiResponse = await this._citaService.obtenerEstadosCitas()
            if (Identificador == "success") {
                this.listaEstadosCitas = Estados
                console.log(this.listaEstadosCitaSesion)
            }
        } catch (error) {

        }
    }
    async agregarPago(item: any) {
        console.log("AgregarPago")
        const dialogRef = this.dialog.open(PagoComponent, {
            width: '500px',
            data: item
        })
        dialogRef.afterClosed().subscribe(result => {
            console.log("result", result)
            if (result) this.ngOnInit();
        })
    }
    registrarDevolucion(item: any) {
        // let estadoPago = (item.EstadoPago==0)?false:true;
        if (item.EstadoCita!="3") {
            return __messageSnackBar(this._matSnackBar, "Debe cancelar la cita para realizar la devolucion")
        }
        const dialogRef = this.dialog.open(DevolucionComponent, {
            width: '500px',
            data: item
        })
        dialogRef.afterClosed().subscribe(result => {
            console.log("result", result.data)
            if (result.data) this.ngOnInit();
        })
    }
    async paginacion(event: any) {
        console.log("event", event)
        await this.listarCitas(event.pageIndex, this.paginatorPageSize);
    }

    verDetallePago(item:any){

    }

}